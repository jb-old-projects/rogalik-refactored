/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#pragma comment (lib, "SDL2")
#include <SDL.h>

class GameTimer
{
public:
	GameTimer();

	void Start();	// Metoda rozpoczynaj�ca zliczanie
	void Stop();	// Metoda zatrzymuj�ca zliczanie
	void Pause();	// Metoda wstrzymuj�ca zliczanie
	void Resume();	// Metoda wznawiaj�ca zliczanie
	Uint32 GetTicks();
	bool Started();
	bool Paused();

private:

	Uint32 startTicks;
	Uint32 pausedTicks;
	bool isPaused;
	bool isStarted;
};