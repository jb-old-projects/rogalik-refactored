/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "Player.h"

Player::Player(std::string bmpName, int playerPosX, int playerPosY, int playerSizeW, int playerSizeH)
{
	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	tileSurface = IMG_LoadPNG_RW(rwop);

	this->playerSizeW = playerSizeW;
	this->playerSizeH = playerSizeH;

	playerAnim.x = 0;
	playerAnim.y = 0;
	playerAnim.w = playerSizeW;
	playerAnim.h = playerSizeH;

	playerPos.x = playerPosX;
	playerPos.y = playerPosY;

	playerRenderRect.x = playerPosX - (playerSizeW / 2);
	playerRenderRect.y = playerPosY - playerSizeH;

	playerSpeed = 3;
	playerHealth = 5;
	playerMoney = 0;

	colliderW = 25;
	colliderH = 8;
	colliderPosX_TMP = 7;
	colliderPosY_TMP = 50;
	posX = playerPos.x + colliderPosX_TMP;
	posY = playerPos.y + colliderPosY_TMP;
}

Player::~Player()
{
	SDL_RWclose(rwop);
	SDL_FreeSurface(tileSurface);
}

void Player::EditSurface(std::string bmpName, int playerSizeW, int playerSizeH)
{
	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	tileSurface = IMG_LoadPNG_RW(rwop);

	this->playerSizeW = playerSizeW;
	this->playerSizeH = playerSizeH;
	playerAnim.w = playerSizeW;
	playerAnim.h = playerSizeH;

	playerRenderRect.x = playerPos.x - (playerSizeW / 2);
	playerRenderRect.y = playerPos.y - playerSizeH;

	playerAnim.x = 0;
	playerAnim.y = 0;
}

SDL_Surface* Player::GetTileSurface()
{
	return tileSurface;
}

SDL_Rect Player::GetPlayerPos()
{
	return playerPos;
}

void Player::SetPlayerPosX(int x)
{
	playerPos.x = x;
}

void Player::SetPlayerPosY(int y)
{
	playerPos.y = y;
}

void Player::SetPlayerRenderRectX(int x)
{
	playerRenderRect.x = x;
}

void Player::SetPlayerRenderRectY(int y)
{
	playerRenderRect.y = y;
}

SDL_Rect Player::GetPlayerAnim()
{
	return playerAnim;
}

SDL_Rect Player::GetPlayerRenderRect()
{
	return playerRenderRect;
}

void Player::SetPlayerAnimX(int x)
{
	playerAnim.x = x;
}

void Player::SetPlayerAnimY(int y)
{
	playerAnim.y = y;
}

int Player::GetPlayerHealth()
{
	return playerHealth;
}

int Player::GetPlayerSpeed()
{
	return playerSpeed;
}

int Player::GetPlayerMoney()
{
	return playerMoney;
}

int Player::GetPlayerSizeW()
{
	return playerSizeW;
}

int Player::GetPlayerSizeH()
{
	return playerSizeH;
}

int Player::GetColliderPosX_TMP()
{
	return colliderPosX_TMP;
}

int Player::GetColliderPosY_TMP()
{
	return colliderPosY_TMP;
}
void Player::SetColliderPosX_TMP(int x)
{
	colliderPosX_TMP = x;
}

void Player::SetColliderPosY_TMP(int y)
{
	colliderPosY_TMP = y;
}

void Player::SetColliderW(int w)
{
	colliderW = w;
}

void Player::SetColliderH(int h)
{
	colliderH = h;
}

int Player::GetColliderW()
{
	return colliderW;
}

int Player::GetColliderH()
{
	return colliderH;
}

void Player::SetPlayerSpeed(int s)
{
	playerSpeed = s;
}

void Player::SetPlayerMoney(int m)
{
	playerMoney = m;
}

void Player::SetPlayerHealth(int h)
{
	playerHealth = h;
}

void Player::SetPlayerAnimationSpeed(int s)
{
	playerAnimationSpeed = s;
}

int Player::GetPlayerAnimationSpeed()
{
	return playerAnimationSpeed;
}

void Player::SetPosX(int x)
{
	posX = x;
}

void Player::SetPosY(int y)
{
	posY = y;
}

int Player::GetPosX()
{
	return posX;
}

int Player::GetPosY()
{
	return posY;
}