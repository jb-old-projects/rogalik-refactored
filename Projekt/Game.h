/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#pragma comment (lib, "SDL2_mixer")
#pragma comment (lib, "SDL2")
#pragma comment (lib, "SDL2_image")
#pragma comment (lib, "SDL2_ttf")

#include "GameWindow.h"
#include "GameMap.h"
#include "AnimatedGameObject.h"
#include "GameObject.h"
#include "Tile.h"
#include "AnimatedTile.h"
#include "GameTimer.h"
#include "Player.h"
#include <SDL2\SDL_image.h>
#include <SDL2\SDL_mixer.h>
#include <SDL2\SDL_ttf.h>

class Game
{
#pragma region Public
public:

#pragma region Internal Enumaration Types

	enum GameStatus
	{
		Menu,
		Gameplay,
		Gamepause,
		End
	};
	enum GameResolution
	{
		RES_800x600,
		RES_1024x768,
		RES_1280x960,
		RES_1440x1080
	};

#pragma endregion

#pragma region Methods

	Game::Game();
	Game::~Game();
	void Init();	// Metoda inicjalizuj�ca
	void GameLoop(GameStatus gStatus);	// P�tla czasu rzeczywistego
	void Draw(GameStatus gStatus); // Metoda rysuj�ca (kafelki)
	void DrawObjects();	// Metoda rysuj�ca (obiekty)
	void MenuDraw();	// Metoda rysuj�ca menu
	void MenuPauseDraw();	// Metoda rysuj�ca menu pauzy
	void Update();	// Metoda aktualizuj�ca dane
	void GameEnding();	// Metoda wy�wietlaj�ca zako�czenie gry
	void Input(SDL_Event event, GameStatus gStatus);	// Metoda sprawdzaj�ca, czy zaszed� jaki� event
	void ChangeResolution(int state);	// Metoda zmieniaj�ca rozdzielczo�� gry
	void AddColliders();	// Metoda dodaj�ca kolidery do obiekt�w
	bool IsCollision();	// Metoda sprawdzaj�ca, czy nast�pi�a kolizja gracza z obiektem
	void LoadGameData();	// Metoda wczytuj�ca wszystkie potrzebne assety
	bool CanWalk(int x);	// Metoda sprawdzaj�ca, czy grasz mo�e si� porusza� po danym kafelku

#pragma endregion

#pragma endregion

#pragma region Private
private:

#pragma region Constants

	const int SCREEN_FPS = 60;
	const int SCREEN_TICK_PER_FRAME = 1000 / SCREEN_FPS;
	const int MUSIC_VOLUME = 10;
	const int SFX_HEART_VOLUME = 80;
	const int SFX_MONEY_VOLUME = 10;
	const int FONT_SIZE = 17;
	const std::string PATH_PREFIX_800_X_600 = "800x600/";
	const std::string PATH_PREFIX_1024_X_768 = "1024x768/";
	const std::string PATH_PREFIX_1280_X_960 = "1280x960/";
	const std::string PATH_PREFIX_1440_X_1080 = "1440x1080/";

#pragma endregion

#pragma region Variables

	GameStatus gStatus = GameStatus::Menu;
	GameResolution gResolution = GameResolution::RES_800x600;
	bool isOpened = false;
	bool wasStarted = 0;
	int countedFrames = 0;

	int menuDotW = 60;
	int menuDotH = 54;

	GameMap* gameMap = NULL;
	GameWindow* gameWindow = NULL;
	Player* hero = NULL;

	Tile* gameFloor = NULL;
	Tile* gameUpWall = NULL;
	Tile* gameLeftWall = NULL;
	Tile* gameRightWall = NULL;
	Tile* gameDownWall = NULL;
	Tile* firstCorner = NULL;
	Tile* secondCorner = NULL;
	Tile* thirdCorner = NULL;
	Tile* fourthCorner = NULL;
	Tile* floorBlackHole = NULL;
	Tile* gameBackground = NULL;

	AnimatedTile* gameUpDoors = NULL;
	AnimatedTile* gameLeftDoors = NULL;
	AnimatedTile* gameRightDoors = NULL;
	AnimatedTile* gameDownDoors = NULL;
	AnimatedTile* gameMenu = NULL;
	AnimatedTile* gameEnd = NULL;
	AnimatedTile* gameMenuDot = NULL;
	AnimatedTile* gameMenuPauseDot = NULL;
	AnimatedTile* gameMenuPause = NULL;
	AnimatedTile* gameResolutions = NULL;
	AnimatedTile* fullscreenOnOff = NULL;
	AnimatedTile* playerStats = NULL;
	AnimatedTile* textBackground = NULL;
	AnimatedGameObject* money = NULL;
	AnimatedGameObject* heart = NULL;

	GameObject* hole_1 = NULL;
	GameObject* hole_2 = NULL;
	GameObject* hole_3 = NULL;
	GameObject* hole_4 = NULL;
	GameObject* hole_5 = NULL;
	GameObject* hole_6 = NULL;
	GameObject* hole_7 = NULL;
	GameObject* hole_8 = NULL;
	GameObject* hole_9 = NULL;
	GameObject* hole_10 = NULL;
	GameObject* hole_11 = NULL;
	GameObject* hole_12 = NULL;
	GameObject* hole_13 = NULL;
	GameObject* hole_14 = NULL;

	TTF_Font *gFont = NULL;

	GameTimer fpsTimer;
	GameTimer capTimer;

	Mix_Music *gMusic = NULL;
	Mix_Music *gGameMusic = NULL;
	Mix_Chunk *gCoin = NULL;
	Mix_Chunk *gHeart = NULL;

#pragma endregion

#pragma endregion
};