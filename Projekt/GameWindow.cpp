/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "GameWindow.h"
#include "Game.h"

GameWindow::GameWindow(std::string windowName, int windowWidth, int windowHeight, bool fullScreen)
{
	gWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN);
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	SDL_RWops *rwop;
	rwop = SDL_RWFromFile("800x600/pngGameIcon.png", "br");
	gWindowIcon = IMG_LoadPNG_RW(rwop);

	SDL_SetWindowIcon(gWindow, gWindowIcon);
}

GameWindow::~GameWindow()
{
	SDL_FreeSurface(gWindowIcon);
	SDL_FreeSurface(gScreenSurface);
	SDL_DestroyWindow(gWindow);
}

SDL_Surface* GameWindow::GetScreenSurface()
{
	return gScreenSurface;
}

SDL_Window* GameWindow::GetWindow()
{
	return gWindow;
}

void GameWindow::SetScreenSurface(SDL_Surface* surface)
{
	gScreenSurface = surface;
}