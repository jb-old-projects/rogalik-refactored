/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#pragma comment (lib, "SDL2")
#pragma comment (lib, "SDL2_image")
#include <SDL_image.h>
#include <SDL.h>
#include <string>

class GameWindow
{
public:
	GameWindow(std::string windowName = "Rogalik", int windowWidth = 792, int windowHeight = 594, bool isFullScreen = false);
	~GameWindow();

	SDL_Window* GetWindow();
	SDL_Surface* GetScreenSurface();

	void SetScreenSurface(SDL_Surface* surface);

private:
	SDL_Window* gWindow = NULL;
	SDL_Surface* gScreenSurface = NULL;
	SDL_Surface* gWindowIcon = NULL;
	std::string windowName;
};