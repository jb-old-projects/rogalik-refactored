/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#include <vector>

class GameMapRoom
{
public:

	struct RoomObjectsInfo
	{
		int roomObjectPosX;
		int roomObjectPosY;
		int roomObjectID;
		int colliderPosX;
		int colliderPosY;
		int colliderW;
		int colliderH;

		RoomObjectsInfo(int x, int y, int id)
		{
			roomObjectID = id;
			roomObjectPosX = x;
			roomObjectPosY = y;
		}
	};

	GameMapRoom(int id, int gameState);
	~GameMapRoom();

	void GenerateHoles(int tab[4], int gStatus);	// Metoda generuj�ca dziury w pokoju
	void GenerateMoney(int gStatus);	// Metoda generuj�ca monety (obiekt gry)
	void GenerateHearts(int gStatus);	// Metoda generuj�ca serca (obiekt gry)
	void EraseRoomObject(int i);	 // Metoda usuwaj�ca obiekt gry z pokoju (przy podniesieniu)

#pragma region Getters

	GameMapRoom** GetDoors();
	int GetRoomMapTileID(int x, int y);
	int GetRoomObjectID(int i);
	int GetRoomObjectsSize();
	int GetRoomObjectPosX(int i);
	int GetRoomObjectPosY(int i);
	int GetRoomObjectColliderPosX(int i);
	int GetRoomObjectColliderPosY(int i);
	int GetRoomObjectColliderWidth(int i);
	int GetRoomObjectColliderHeight(int i);

#pragma endregion

#pragma region Setters

	void SetRoomObjectColliderPosX(int i, int x);
	void SetRoomObjectColliderPosY(int i, int y);
	void SetRoomObjectPosX(int i, int x);
	void SetRoomObjectPosY(int i, int y);
	void SetRoomObjectColliderWidth(int i, int w);
	void SetRoomObjectColliderHeight(int i, int h);

#pragma endregion

private:

	GameMapRoom* doors[4]{ NULL };
	std::vector<RoomObjectsInfo> roomObjects;
	int roomMap[11][11];
	int id = 99;
};