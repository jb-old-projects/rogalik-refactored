/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#include <stdlib.h>
#include <time.h>
#include "Tile.h"
#include "GameObject.h"
#include "AnimatedGameObject.h"
#include "GameMapRoom.h"

class GameMap
{
public:

	GameMap();
	~GameMap();

	static void SetRoomAmount(int a);
	static int GetRoomAmount();

	GameMapRoom* GetCurrentRoom();
	int GetTileSizeW();
	int GetTileSizeH();
	int GetTileAmount();
	int GetMapH();
	int GetMapW();
	int GetMapSize();

	void SetCurrentRoom(GameMapRoom* room);
	void SetTileSizeW(int w);
	void SetTileSizeH(int h);
	void SetTileAmount(int a);
	void SetMapH(int h);
	void SetMapW(int w);
	void SetMapSize(int s);

private:

	static int roomAmount;
	GameMapRoom* currentRoom = NULL;

	int tileSizeW;
	int tileSizeH;
	int tileAmount;

	int mapH;
	int mapW;
	int mapSize;
};