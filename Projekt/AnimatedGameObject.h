/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#include "GameObject.h"

class AnimatedGameObject : public GameObject
{
public:
	const int ANIMATION_STATE_COUNTER_CAP = 10;

	AnimatedGameObject(GameObjectID id, GameObjectType goType, std::string bmpName, int goWidth, int goHeight);
	void EditSurface(std::string bmpName, int width, int height);	// Metoda zmieniaj�ca surface przy zmianie rozdzielczo�ci w grze

	SDL_Rect GetGameObjectAnim();
	int GetAnimationStateCounter();
	int GetGameObjectAnimPosX();
	int GetGameObjectAnimPosY();

	void SetAnimationStateCounter(int animationStateCounter);
	void SetGameObjectAnimPosX(int x);
	void SetGameObjectAnimPosY(int y);

private:
	int animationStateCounter = 1;
	SDL_Rect gameObjectAnim;
};