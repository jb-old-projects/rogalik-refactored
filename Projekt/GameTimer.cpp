/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "GameTimer.h"

GameTimer::GameTimer()
{
	startTicks = 0;
	pausedTicks = 0;

	isPaused = false;
	isStarted = false;
}

void GameTimer::Start()
{
	isStarted = true;
	isPaused = false;

	startTicks = SDL_GetTicks();
	pausedTicks = 0;
}

void GameTimer::Stop()
{
	isStarted = false;
	isPaused = false;

	startTicks = 0;
	pausedTicks = 0;
}

void GameTimer::Pause()
{
	if (isStarted && !isPaused)
	{
		isPaused = true;
		pausedTicks = SDL_GetTicks() - startTicks;
		startTicks = 0;
	}
}

void GameTimer::Resume()
{
	if (isStarted && isPaused)
	{
		isPaused = false;
		startTicks = SDL_GetTicks() - pausedTicks;
		pausedTicks = 0;
	}
}

Uint32 GameTimer::GetTicks()
{
	Uint32 time = 0;

	if (isStarted)
	{
		if (isPaused)
		{
			time = pausedTicks;
		}
		else
		{
			time = SDL_GetTicks() - startTicks;
		}
	}

	return time;
}

bool GameTimer::Started()
{
	return isStarted;
}

bool GameTimer::Paused()
{
	return isPaused && isStarted;
}