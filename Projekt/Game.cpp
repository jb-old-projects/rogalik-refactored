/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "Game.h"

Game::Game()
{
	Init();
	LoadGameData();
	AddColliders();
	fpsTimer.Start();

	isOpened = true;
	GameLoop(gStatus);
}

Game::~Game()
{
	delete gameFloor;
	delete gameUpWall;
	delete gameLeftWall;
	delete gameRightWall;
	delete gameDownWall;
	delete firstCorner;
	delete secondCorner;
	delete thirdCorner;
	delete fourthCorner;
	delete gameUpDoors;
	delete gameLeftDoors;
	delete gameRightDoors;
	delete gameDownDoors;
	delete gameMenu;
	delete gameEnd;
	delete gameMenuDot;
	delete gameMenuPauseDot;
	delete gameMenuPause;
	delete gameResolutions;
	delete fullscreenOnOff;
	delete hero;

	delete floorBlackHole;
	delete gameBackground;
	delete playerStats;
	delete textBackground;
	delete money;
	delete heart;
	delete hole_1;
	delete hole_2;
	delete hole_3;
	delete hole_4;
	delete 	hole_5;
	delete 	hole_6;
	delete 	hole_7;
	delete 	hole_8;
	delete 	hole_9;
	delete 	hole_10;
	delete 	hole_11;
	delete 	hole_12;
	delete 	hole_13;
	delete hole_14;

	delete gameMap;
	delete gameWindow;

	fpsTimer.Stop();
	capTimer.Stop();

	Mix_FreeMusic(gMusic);
	Mix_FreeMusic(gGameMusic);
	Mix_FreeChunk(gCoin);
	Mix_FreeChunk(gHeart);
	Mix_CloseAudio();
	TTF_CloseFont(gFont);

	IMG_Quit();
	TTF_Quit();
	Mix_Quit();
	SDL_Quit();
}

void Game::Init()
{
	gameWindow = new GameWindow();
	gameMap = new GameMap();
}

void Game::LoadGameData()
{
	hero = new Player(PATH_PREFIX_800_X_600 + "pngHero.png", gameMap->GetMapW() / 2, gameMap->GetMapH() / 2, 39, 58);
	gameFloor = new Tile(Tile::Floor, true, PATH_PREFIX_800_X_600 + "bmpFloor2.bmp");
	gameUpWall = new Tile(Tile::UpperWall, false, PATH_PREFIX_800_X_600 + "bmpUpsideWall.bmp");
	gameLeftWall = new Tile(Tile::LeftWall, false, PATH_PREFIX_800_X_600 + "bmpLeftsideWall.bmp");
	gameRightWall = new Tile(Tile::RightWall, false, PATH_PREFIX_800_X_600 + "bmpRightsideWall.bmp");
	gameDownWall = new Tile(Tile::BottomWall, false, PATH_PREFIX_800_X_600 + "bmpDownsideWall.bmp");
	firstCorner = new Tile(Tile::UpperCornerL, false, PATH_PREFIX_800_X_600 + "FirstCorner.bmp");
	secondCorner = new Tile(Tile::UpperCornerR, false, PATH_PREFIX_800_X_600 + "SecondCorner.bmp");
	thirdCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_800_X_600 + "ThirdCorner.bmp");
	fourthCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_800_X_600 + "FourthCorner.bmp");
	gameUpDoors = new AnimatedTile(AnimatedTile::UpperDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	gameLeftDoors = new AnimatedTile(AnimatedTile::LeftDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	gameRightDoors = new AnimatedTile(AnimatedTile::RightDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	gameDownDoors = new AnimatedTile(AnimatedTile::BottomDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	gameMenu = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_800_X_600 + "pngGameMenu.png", 0, 0, gameMap->GetMapW(), gameMap->GetMapH());
	gameEnd = new AnimatedTile(AnimatedTile::Ending, false, PATH_PREFIX_800_X_600 + "pngGameEnding.png", 0, 0, gameMap->GetMapW(), 1270);
	gameMenuDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_800_X_600 + "pngGameMenuDot.png", 0, 0, menuDotW, menuDotH);
	gameMenuPauseDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_800_X_600 + "pngGameMenuPauseDot.png", 0, 0, menuDotW, menuDotH);
	gameMenuPause = new AnimatedTile(AnimatedTile::Pause, false, PATH_PREFIX_800_X_600 + "pngGameMenuPause.png", 0, 0, gameMap->GetMapW(), gameMap->GetMapH());
	gameResolutions = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_800_X_600 + "pngGameResolutions.png", 0, 0, 408, 90);
	fullscreenOnOff = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_800_X_600 + "pngFulscreenOnOff.png", 0, 75, 324, 75);
	floorBlackHole = new Tile(Tile::Hole, false, PATH_PREFIX_800_X_600 + "bmpBlackHole.bmp");
	gameBackground = new Tile(Tile::Floor, false, PATH_PREFIX_800_X_600 + "GameBackground.bmp");
	playerStats = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_800_X_600 + "pngStats.png", 0, 0, 72, 27);
	textBackground = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_800_X_600 + "pngStatsTextBackground.png", 0, 0, 72, 27);
	/********************************************************************************************/
	hole_1 = new GameObject(GameObject::e_hole_1, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_1.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_2 = new GameObject(GameObject::e_hole_2, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_2.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_3 = new GameObject(GameObject::e_hole_3, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_3.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_4 = new GameObject(GameObject::e_hole_4, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_4.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_5 = new GameObject(GameObject::e_hole_5, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_5.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_6 = new GameObject(GameObject::e_hole_6, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_6.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_7 = new GameObject(GameObject::e_hole_7, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_7.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_8 = new GameObject(GameObject::e_hole_8, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_8.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_9 = new GameObject(GameObject::e_hole_9, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_9.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_10 = new GameObject(GameObject::e_hole_10, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_10.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_11 = new GameObject(GameObject::e_hole_11, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_11.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_12 = new GameObject(GameObject::e_hole_12, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "Hole_12.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_13 = new GameObject(GameObject::e_hole_13, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "pngHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
	hole_14 = new GameObject(GameObject::e_hole_14, GameObject::e_neutral, PATH_PREFIX_800_X_600 + "pngBigHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());

	money = new AnimatedGameObject(AnimatedGameObject::e_money, AnimatedGameObject::e_neutral, PATH_PREFIX_800_X_600 + "pngMoney.png", 32, 32);
	heart = new AnimatedGameObject(AnimatedGameObject::e_heart, AnimatedGameObject::e_neutral, PATH_PREFIX_800_X_600 + "pngHeart.png", 24, 24);

	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	gMusic = Mix_LoadMUS("music/menumusic.mp3");
	printf("%s", Mix_GetError());
	gGameMusic = Mix_LoadMUS("music/gamemusic.mp3");

	gCoin = Mix_LoadWAV("music/coin.mp3");
	gHeart = Mix_LoadWAV("music/heart.mp3");

	Mix_VolumeChunk(gHeart, SFX_HEART_VOLUME);
	Mix_VolumeChunk(gCoin, SFX_MONEY_VOLUME);
	Mix_VolumeMusic(MUSIC_VOLUME);

	TTF_Init();
	gFont = TTF_OpenFont("fonts/BEECH___.ttf", FONT_SIZE);
}

void Game::GameLoop(Game::GameStatus gStatus)
{
	SDL_Event event;
	int resChangeCounter = 0;
	int fullscreenChangeCounter = 0;

	while (isOpened)
	{
		if (gStatus == Gameplay)
		{
			wasStarted = true;
			capTimer.Start();

			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
				case SDL_QUIT:
					isOpened = false;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
					case SDLK_ESCAPE:
					{
						gStatus = Gamepause;

						gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.21);
						gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.36);
						break;
					}
					}
					break;
				}
			}

			Input(event, gStatus);

			Draw(gStatus);

			Update();
		}

		/******************************************            MAIN MENU            ******************************************/

		else if (gStatus == Menu)
		{
			int tmp = -99;
			Mix_VolumeMusic(10);
			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
				case SDL_QUIT:
					isOpened = false;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
					case SDLK_ESCAPE:
					{
						if (gameMenu->GetTileRect().y == gameMap->GetMapH())
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.72))
								GameEnding();
							else
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.72);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.35);
							}
						}

						if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 2)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.81))
							{
								gameMenu->SetTileRectY(gameMap->GetMapH());
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.21);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.21);
							}
							else
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.81);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.34);
							}
						}

						if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 3)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.87))
							{
								gameMenu->SetTileRectY(gameMap->GetMapH() * 2);
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.1);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);
							}
							else
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.87);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.31);
							}
						}

						break;
					}
					case SDLK_q:
					{
						GameEnding();
						break;
					}
					case SDLK_RETURN:
					{

						if (gameMenu->GetTileRect().y == 0)
						{
							gameMenu->SetTileRectY(gameMap->GetMapH());
							gameMenuDot->SetTileRectX(gameMap->GetMapW() * 0.21);
							gameMenuDot->SetTileRectY(gameMap->GetMapH() * 0.21);
						}

						else if (gameMenu->GetTileRect().y == gameMap->GetMapH())
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.21))
							{
								gStatus = Gameplay;
								Mix_PauseMusic();
								Mix_PlayMusic(gGameMusic, -1);
								Mix_VolumeMusic(20);
								SDL_BlitSurface(gameBackground->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), NULL);
								SDL_UpdateWindowSurface(gameWindow->GetWindow());
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.47))
							{
								gameMenu->SetTileRectY(gameMap->GetMapH() * 2);
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.1);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.72))
								GameEnding();
						}
						else if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 2)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.1))
							{
								gameMenu->SetTileRectY(gameMap->GetMapH() * 3);
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.13);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.16);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.81))
							{
								if (wasStarted == false)
								{
									gameMenu->SetTileRectY(gameMap->GetMapH());
									gameMenuDot->SetTileRectX(gameMap->GetMapW() * 0.21);
									gameMenuDot->SetTileRectY(gameMap->GetMapH() * 0.21);
								}
								else
								{
									gStatus = Gamepause;
									Mix_PauseMusic();
									Mix_PlayMusic(gGameMusic, -1);
									Mix_VolumeMusic(20);
									gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.53);
									gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.31);
								}
							}
						}
						else if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 3)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.87))
							{
								gameMenu->SetTileRectY(gameMap->GetMapH() * 2);
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.1);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);

								int tmp2 = -99;

								if (fullscreenOnOff->GetTileRect().y == 0)
								{
									tmp2 = 1;
								}
								else
								{
									tmp2 = 0;
								}

								if (tmp != 0)
								{
									if (tmp2 == 1)
									{
										if (resChangeCounter == 0 && fullscreenChangeCounter == 0)
										{
											continue;
										}

										ChangeResolution((gameResolutions->GetTileRect().y / gameResolutions->GetTileRect().h));
										resChangeCounter = 0;
										fullscreenChangeCounter = 0;
										fullscreenOnOff->SetTileRectY(0);
										SDL_SetWindowFullscreen(gameWindow->GetWindow(), SDL_WINDOW_FULLSCREEN);
										gameWindow->SetScreenSurface(SDL_GetWindowSurface(gameWindow->GetWindow()));

									}
									else if (tmp2 == 0)
									{
										if (resChangeCounter == 0)
										{
											continue;
										}

										ChangeResolution((gameResolutions->GetTileRect().y / gameResolutions->GetTileRect().h));
										resChangeCounter = 0;
										fullscreenChangeCounter = 0;

									}
								}
							}
						}

						break;
					}
					case SDLK_DOWN:
					{
						if (gameMenu->GetTileRect().y == gameMap->GetMapH())
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.21))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.47);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.27);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.47))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.72);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.35);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.72))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.21);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.21);
							}
						}

						else if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 2)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.1))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.35);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.35))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.58);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.24);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.58))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.81);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.34);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.81))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.1);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);
							}
						}

						else if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 3)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.13))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.57);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.225);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.57))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.87);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.31);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.87))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.13);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.16);
							}
						}

						break;
					}
					case SDLK_UP:
					{
						if (gameMenu->GetTileRect().y == gameMap->GetMapH())
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.21))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.72);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.35);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.47))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.21);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.21);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.72))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.47);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.27);
							}
						}

						else if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 2)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.1))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.81);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.34);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.35))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.1);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.58))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.35);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.81))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.58);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.24);
							}
						}

						else if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 3)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.13))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.87);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.31);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.57))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.13);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.16);
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.87))
							{
								gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.57);
								gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.225);
							}
						}

						break;
					}

					case SDLK_LEFT:
					{
						if (gameResolutions->GetTileRect().y == 0)
							tmp = 0;

						if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 3)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.13))
							{
								if (gameResolutions->GetTileRect().y == 0)
								{
									gameResolutions->SetTileRectY(gameResolutions->GetTileRect().h * 3);
									resChangeCounter--;
									if (resChangeCounter == -4)
										resChangeCounter = 0;
								}
								else
								{
									gameResolutions->SetTileRectY(gameResolutions->GetTileRect().y - gameResolutions->GetTileRect().h);
									resChangeCounter--;

									if (resChangeCounter == -4)
									{
										resChangeCounter = 0;
									}

									tmp--;
								}
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.57))
							{
								if (fullscreenOnOff->GetTileRect().y == fullscreenOnOff->GetTileRect().h)
								{
									fullscreenOnOff->SetTileRectY(0);
									fullscreenChangeCounter--;

									if (fullscreenChangeCounter == -2)
										fullscreenChangeCounter = 0;
								}
							}
						}

						break;
					}

					case SDLK_RIGHT:
					{
						if (gameResolutions->GetTileRect().y == 0)
							tmp = 0;

						if (gameMenu->GetTileRect().y == gameMap->GetMapH() * 3)
						{
							if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.13))
							{
								if (gameResolutions->GetTileRect().y == gameResolutions->GetTileRect().h * 3)
								{
									gameResolutions->SetTileRectY(0);
									resChangeCounter++;
									if (resChangeCounter == 4)
										resChangeCounter = 0;
								}

								else
								{
									gameResolutions->SetTileRectY(gameResolutions->GetTileRect().y + gameResolutions->GetTileRect().h);
									resChangeCounter++;

									if (resChangeCounter == 4)
										resChangeCounter = 0;
									tmp++;
								}
							}
							else if (gameMenuDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.57))
							{
								if (fullscreenOnOff->GetTileRect().y == 0)
								{
									fullscreenOnOff->SetTileRectY(fullscreenOnOff->GetTileRect().h);
									fullscreenChangeCounter++;

									if (fullscreenChangeCounter == 2)
										fullscreenChangeCounter = 0;
								}
							}
						}
						break;
					}
					}
					break;
				}
			}

			if (Mix_PlayingMusic() == 0)
				Mix_PlayMusic(gMusic, 0);

			MenuDraw();
		}

		else if (gStatus == Gamepause)
		{
			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
				case SDL_QUIT:
					isOpened = false;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
					case SDLK_ESCAPE:
					{
						gStatus = Gameplay;
						break;
					}
					case SDLK_q:
					{
						GameEnding();
						break;
					}
					case SDLK_RETURN:
					{
						if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.36))
							gStatus = Gameplay;
						else if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.53))
						{
							gStatus = Menu;
							Mix_HaltMusic();
							Mix_PlayMusic(gMusic, -1);
							Mix_VolumeMusic(10);
							gameMenu->SetTileRectY(gameMap->GetMapH() * 2);
							gameMenuDot->SetTileRectY(gameMap->GetMapH()*0.1);
							gameMenuDot->SetTileRectX(gameMap->GetMapW()*0.32);
						}
						else if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.7))
							GameEnding();

						break;
					}
					case SDLK_DOWN:
					{
						if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.36))
						{
							gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.53);
							gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.31);
						}
						else if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.53))
						{
							gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.7);
							gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.36);
						}
						else if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.7))
						{
							gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.21);
							gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.36);
						}
						break;
					}
					case SDLK_UP:
					{
						if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.36))
						{
							gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.7);
							gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.36);
						}
						else if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.53))
						{
							gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.21);
							gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.36);
						}
						else if (gameMenuPauseDot->GetTileRect().y == (int)(gameMap->GetMapH()*0.7))
						{
							gameMenuPauseDot->SetTileRectY(gameMap->GetMapH()*0.53);
							gameMenuPauseDot->SetTileRectX(gameMap->GetMapW()*0.31);
						}

						break;
					}
					}
					break;
				}
			}
			MenuPauseDraw();
		}
	}
}

void Game::Update()
{
	if (Mix_PlayingMusic() == 0)
	{
		Mix_PlayMusic(gGameMusic, -1);
	}

	float avgFPS = countedFrames / (fpsTimer.GetTicks() / 1000.0f);

	if (avgFPS > 2000000)
	{
		avgFPS = 0;
	}

	int frameTicks = capTimer.GetTicks();

	if (frameTicks < SCREEN_TICK_PER_FRAME)
	{
		SDL_Delay(SCREEN_TICK_PER_FRAME - frameTicks);
	}
}

void Game::Draw(GameStatus gStatus)
{
	SDL_Rect Render_TMP;
	Render_TMP.x = 0;
	Render_TMP.y = 0;

	SDL_Color TextColor = { 255,255,255 };

	SDL_BlitSurface(gameBackground->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), NULL);

	for (int i = 0; i < gameMap->GetTileAmount(); i++)
	{
		for (int j = 0; j < gameMap->GetTileAmount(); j++)
		{
			if ((gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) > 4 && gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) < 9) || gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 0)  // floor
				SDL_BlitSurface(gameFloor->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 1) // up wall
				SDL_BlitSurface(gameUpWall->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 2) // left wall
				SDL_BlitSurface(gameLeftWall->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 3) // right wall
				SDL_BlitSurface(gameRightWall->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 4) // down wall
				SDL_BlitSurface(gameDownWall->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 9) // up-left corner
				SDL_BlitSurface(firstCorner->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 10) // up-right corner
				SDL_BlitSurface(secondCorner->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 11) // down-left corner
				SDL_BlitSurface(thirdCorner->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 12) // down-right corner
				SDL_BlitSurface(fourthCorner->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
			if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 5) // up doors
			{
				gameUpDoors->SetTileRectY(0);
				if (gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH()) == 5 && hero->GetPlayerPos().y < 92)
				{
					gameUpDoors->SetTileRectX(gameMap->GetTileSizeW());
					SDL_BlitSurface(gameUpDoors->GetTileSurface(), &gameUpDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
				else
				{
					gameUpDoors->SetTileRectX(0);
					SDL_BlitSurface(gameUpDoors->GetTileSurface(), &gameUpDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
			}

			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 6) // left doors
			{
				gameLeftDoors->SetTileRectY(gameMap->GetTileSizeH());
				if (gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH()) == 6 && hero->GetPlayerPos().x < 92)
				{
					gameLeftDoors->SetTileRectX(gameMap->GetTileSizeW());
					SDL_BlitSurface(gameLeftDoors->GetTileSurface(), &gameLeftDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
				else
				{
					gameLeftDoors->SetTileRectX(0);
					SDL_BlitSurface(gameLeftDoors->GetTileSurface(), &gameLeftDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
			}

			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 7) // right doors
			{
				gameRightDoors->SetTileRectY(gameMap->GetTileSizeH() * 2);
				if (gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH()) == 7 && hero->GetPlayerPos().x > 458)
				{
					gameRightDoors->SetTileRectX(gameMap->GetTileSizeW());
					SDL_BlitSurface(gameRightDoors->GetTileSurface(), &gameRightDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
				else
				{
					gameRightDoors->SetTileRectX(0);
					SDL_BlitSurface(gameRightDoors->GetTileSurface(), &gameRightDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
			}

			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 8) // down doors
			{
				gameDownDoors->SetTileRectY(gameMap->GetTileSizeH() * 3);
				if (gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH()) == 8 && hero->GetPlayerPos().y > 458)
				{
					gameDownDoors->SetTileRectX(gameMap->GetTileSizeW());
					SDL_BlitSurface(gameDownDoors->GetTileSurface(), &gameDownDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
				else
				{
					gameDownDoors->SetTileRectX(0);
					SDL_BlitSurface(gameDownDoors->GetTileSurface(), &gameDownDoors->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP);
				}
			}
			else if (gameMap->GetCurrentRoom()->GetRoomMapTileID(j, i) == 32)
				SDL_BlitSurface(floorBlackHole->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);

			Render_TMP.x = Render_TMP.x + gameMap->GetTileSizeW();
		}

		Render_TMP.x = 0;
		Render_TMP.y = Render_TMP.y + gameMap->GetTileSizeH();
	}
	DrawObjects();
	hero->SetPlayerRenderRectX(hero->GetPlayerPos().x - (hero->GetPlayerSizeW() / 2));
	hero->SetPlayerRenderRectY(hero->GetPlayerPos().y - hero->GetPlayerSizeH());
	SDL_BlitSurface(hero->GetTileSurface(), &hero->GetPlayerAnim(), gameWindow->GetScreenSurface(), &hero->GetPlayerRenderRect());
	Render_TMP.x = gameMap->GetTileSizeW();
	Render_TMP.y = gameMap->GetTileSizeH() / 2;
	SDL_BlitSurface(playerStats->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
	Render_TMP.x += gameMap->GetTileSizeW() * 0.625;
	Render_TMP.y += gameMap->GetTileSizeH() * 0.11;
	char TMP_Stats[32];
	_itoa_s(hero->GetPlayerHealth(), TMP_Stats, 10);
	textBackground->SetTileSurface(TTF_RenderText_Solid(gFont, TMP_Stats, TextColor));
	SDL_BlitSurface(textBackground->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);
	Render_TMP.x += gameMap->GetTileSizeW()*0.9;
	_itoa_s(hero->GetPlayerMoney(), TMP_Stats, 10);
	textBackground->SetTileSurface(TTF_RenderText_Solid(gFont, TMP_Stats, TextColor));
	SDL_BlitSurface(textBackground->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &Render_TMP);

	if (gStatus == Gameplay)
	{
		SDL_UpdateWindowSurface(gameWindow->GetWindow());
	}
}

void Game::DrawObjects() // funkcja odpowiedzialna za odpowiednie wy�wietlanie obiekt�w (np. monet)
{
	SDL_Rect renderRect;
	renderRect.x = 0;
	renderRect.y = 0;

	for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
	{
		switch (gameMap->GetCurrentRoom()->GetRoomObjectID(i))
		{
		case AnimatedGameObject::e_money:
		{
			money->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			money->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = money->GetRect().x - money->GetRect().w / 2;
			renderRect.y = money->GetRect().y - money->GetRect().w;
			SDL_BlitSurface(money->GetSurface(), &money->GetGameObjectAnim(), gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_heart:
		{
			heart->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			heart->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = heart->GetRect().x - heart->GetRect().w / 2;
			renderRect.y = heart->GetRect().y - heart->GetRect().w;
			SDL_BlitSurface(heart->GetSurface(), &heart->GetGameObjectAnim(), gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_1:
		{
			hole_1->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_1->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_1->GetRect().x;
			renderRect.y = hole_1->GetRect().y;
			SDL_BlitSurface(hole_1->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_2:
		{
			hole_2->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_2->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_2->GetRect().x;
			renderRect.y = hole_2->GetRect().y;
			SDL_BlitSurface(hole_2->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_3:
		{
			hole_3->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_3->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_3->GetRect().x;
			renderRect.y = hole_3->GetRect().y;
			SDL_BlitSurface(hole_3->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_4:
		{
			hole_4->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_4->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_4->GetRect().x;
			renderRect.y = hole_4->GetRect().y;
			SDL_BlitSurface(hole_4->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_5:
		{
			hole_5->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_5->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_5->GetRect().x;
			renderRect.y = hole_5->GetRect().y;
			SDL_BlitSurface(hole_5->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_6:
		{
			hole_6->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_6->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_6->GetRect().x;
			renderRect.y = hole_6->GetRect().y;
			SDL_BlitSurface(hole_6->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_7:
		{
			hole_7->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_7->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_7->GetRect().x;
			renderRect.y = hole_7->GetRect().y;
			SDL_BlitSurface(hole_7->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_8:
		{
			hole_8->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_8->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_8->GetRect().x;
			renderRect.y = hole_8->GetRect().y;
			SDL_BlitSurface(hole_8->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_9:
		{
			hole_9->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_9->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_9->GetRect().x;
			renderRect.y = hole_9->GetRect().y;
			SDL_BlitSurface(hole_9->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_10:
		{
			hole_10->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_10->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_10->GetRect().x;
			renderRect.y = hole_10->GetRect().y;
			SDL_BlitSurface(hole_10->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_11:
		{
			hole_11->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_11->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_11->GetRect().x;
			renderRect.y = hole_11->GetRect().y;
			SDL_BlitSurface(hole_11->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_12:
		{
			hole_12->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_12->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_12->GetRect().x;
			renderRect.y = hole_12->GetRect().y;
			SDL_BlitSurface(hole_12->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_13:
		{
			hole_13->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_13->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_13->GetRect().x;
			renderRect.y = hole_13->GetRect().y;
			SDL_BlitSurface(hole_13->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		case AnimatedGameObject::e_hole_14:
		{
			hole_14->SetRectX(gameMap->GetCurrentRoom()->GetRoomObjectPosX(i));
			hole_14->SetRectY(gameMap->GetCurrentRoom()->GetRoomObjectPosY(i));
			renderRect.x = hole_14->GetRect().x;
			renderRect.y = hole_14->GetRect().y;
			SDL_BlitSurface(hole_14->GetSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
			break;
		}
		}
	}

	money->SetAnimationStateCounter(money->GetAnimationStateCounter() + 1);
	heart->SetAnimationStateCounter(heart->GetAnimationStateCounter() + 1);

	if (money->GetAnimationStateCounter() == money->ANIMATION_STATE_COUNTER_CAP)
	{
		money->SetGameObjectAnimPosX(money->GetGameObjectAnimPosX() + money->GetRect().w);
		money->SetAnimationStateCounter(1);
	}

	if (money->GetGameObjectAnim().x > money->GetRect().w * 7)
	{
		money->SetGameObjectAnimPosX(0);
	}

	if (heart->GetAnimationStateCounter() == heart->ANIMATION_STATE_COUNTER_CAP)
	{
		heart->SetGameObjectAnimPosX(heart->GetGameObjectAnimPosX() + heart->GetRect().w);
		heart->SetAnimationStateCounter(1);
	}

	if (heart->GetGameObjectAnim().x > heart->GetRect().w * 5)
	{
		heart->SetGameObjectAnimPosX(0);
	}
}

void Game::Input(SDL_Event event, Game::GameStatus gStatus)
{
	const Uint8* currentKeyState = SDL_GetKeyboardState(NULL);

	if (currentKeyState[SDL_SCANCODE_LEFT])
	{
		hero->SetPlayerAnimationSpeed(hero->GetPlayerAnimationSpeed() + 1);

		if (hero->GetPlayerAnimationSpeed() == (int)((hero->GetPlayerSpeed() * 5) / (gResolution + 1)))
		{
			hero->SetPlayerAnimX(hero->GetPlayerAnim().x + hero->GetPlayerSizeW());
			hero->SetPlayerAnimationSpeed(1);
		}

		hero->SetPlayerAnimY(hero->GetPlayerSizeH() * 3);

		if (hero->GetPlayerAnim().x > hero->GetPlayerSizeW() * 3)
		{
			hero->SetPlayerAnimX(0);
		}

		hero->SetPlayerPosX(hero->GetPlayerPos().x - hero->GetPlayerSpeed());

		IsCollision();

		if (!CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			hero->SetPlayerPosX(((int)(hero->GetPlayerPos().x / gameMap->GetTileSizeW())) * gameMap->GetTileSizeW() + gameMap->GetTileSizeW() + 1);
		}
		else if (CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			if (hero->GetPlayerPos().x < gameMap->GetTileSizeW())
			{
				if (hero->GetPlayerPos().x < (gameMap->GetTileSizeW() / 4))
				{
					if (gameMap->GetCurrentRoom()->GetDoors()[1] == NULL)
					{
						gameMap->GetCurrentRoom()->GetDoors()[1] = new GameMapRoom(7, (int)gResolution);

						gameMap->GetCurrentRoom()->GetDoors()[1]->GetDoors()[2] = new GameMapRoom(99, (int)gResolution);
						gameMap->GetCurrentRoom()->GetDoors()[1]->GetDoors()[2] = gameMap->GetCurrentRoom();
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[1]);
						AddColliders();
						gameLeftDoors->SetTileRectX(0);
						hero->SetPlayerPosX((gameMap->GetMapW() - (gameMap->GetTileSizeW() / 3)));
					}
					else
					{
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[1]);
						gameLeftDoors->SetTileRectX(0);
						hero->SetPlayerPosX((gameMap->GetMapW() - (gameMap->GetTileSizeW() / 3)));
					}
				}
			}
		}

		hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
	}
	if (currentKeyState[SDL_SCANCODE_RIGHT])
	{
		hero->SetPlayerAnimationSpeed(hero->GetPlayerAnimationSpeed() + 1);

		if (hero->GetPlayerAnimationSpeed() == (int)((hero->GetPlayerSpeed() * 5) / (gResolution + 1)))
		{
			hero->SetPlayerAnimX(hero->GetPlayerAnim().x + hero->GetPlayerSizeW());
			hero->SetPlayerAnimationSpeed(1);
		}

		hero->SetPlayerAnimY(hero->GetPlayerSizeH() * 2);

		if (hero->GetPlayerAnim().x > hero->GetPlayerSizeW() * 3)
		{
			hero->SetPlayerAnimX(0);
		}

		hero->SetPlayerPosX(hero->GetPlayerPos().x + hero->GetPlayerSpeed());
		IsCollision();

		if (!CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			hero->SetPlayerPosX(((int)(hero->GetPlayerPos().x / gameMap->GetTileSizeW())) * gameMap->GetTileSizeW() - 1);
		}
		else if (CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			if (hero->GetPlayerPos().x > (gameMap->GetMapW() - gameMap->GetTileSizeW() / 3))
			{
				if (hero->GetPlayerPos().x > (gameMap->GetMapW() - (gameMap->GetTileSizeW() / 3)))
				{
					if (gameMap->GetCurrentRoom()->GetDoors()[2] == NULL)
					{
						gameMap->GetCurrentRoom()->GetDoors()[2] = new GameMapRoom(6, (int)gResolution);
						gameMap->GetCurrentRoom()->GetDoors()[2]->GetDoors()[1] = new GameMapRoom(99, (int)gResolution);
						gameMap->GetCurrentRoom()->GetDoors()[2]->GetDoors()[1] = gameMap->GetCurrentRoom();
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[2]);
						AddColliders();
						gameRightDoors->SetTileRectX(0);
						hero->SetPlayerPosX(gameMap->GetTileSizeW() / 4);
					}
					else
					{
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[2]);
						gameRightDoors->SetTileRectX(0);
						hero->SetPlayerPosX(gameMap->GetTileSizeW() / 4);
					}
				}

			}
		}

		hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
	}
	if (currentKeyState[SDL_SCANCODE_UP])
	{
		hero->SetPlayerAnimationSpeed(hero->GetPlayerAnimationSpeed() + 1);

		if (hero->GetPlayerAnimationSpeed() == (int)((hero->GetPlayerSpeed() * 5) / (gResolution + 1)))
		{
			hero->SetPlayerAnimX(hero->GetPlayerAnim().x + hero->GetPlayerSizeW());
			hero->SetPlayerAnimationSpeed(1);
		}

		hero->SetPlayerAnimY(hero->GetPlayerSizeH());

		if (hero->GetPlayerAnim().x > hero->GetPlayerSizeW() * 3)
		{
			hero->SetPlayerAnimX(0);
		}

		hero->SetPlayerPosY(hero->GetPlayerPos().y - hero->GetPlayerSpeed());
		IsCollision();

		if (!CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			hero->SetPlayerPosY(((int)(hero->GetPlayerPos().y / gameMap->GetTileSizeH()))*gameMap->GetTileSizeH() + gameMap->GetTileSizeH() + 1);
		}
		else if (CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			if (hero->GetPlayerPos().y < gameMap->GetTileSizeH())
			{
				if (hero->GetPlayerPos().y < (gameMap->GetTileSizeH() / 3))
				{
					if (gameMap->GetCurrentRoom()->GetDoors()[0] == NULL)
					{
						gameMap->GetCurrentRoom()->GetDoors()[0] = new GameMapRoom(8, (int)gResolution);
						gameMap->GetCurrentRoom()->GetDoors()[0]->GetDoors()[3] = new GameMapRoom(99, (int)gResolution);
						gameMap->GetCurrentRoom()->GetDoors()[0]->GetDoors()[3] = gameMap->GetCurrentRoom();
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[0]);
						AddColliders();
						gameUpDoors->SetTileRectX(0);

						hero->SetPlayerPosY(gameMap->GetMapH() - (gameMap->GetTileSizeH() / 3));
					}
					else
					{
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[0]);
						gameUpDoors->SetTileRectX(0);
						hero->SetPlayerPosY(gameMap->GetMapH() - (gameMap->GetTileSizeH() / 3));
					}
				}
			}
		}

		hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
	}
	if (currentKeyState[SDL_SCANCODE_DOWN])
	{
		hero->SetPlayerAnimationSpeed(hero->GetPlayerAnimationSpeed() + 1);

		if (hero->GetPlayerAnimationSpeed() == (int)((hero->GetPlayerSpeed() * 5) / (gResolution + 1)))
		{
			hero->SetPlayerAnimX(hero->GetPlayerAnim().x + hero->GetPlayerSizeW());
			hero->SetPlayerAnimationSpeed(1);
		}

		hero->SetPlayerAnimY(0);

		if (hero->GetPlayerAnim().x > hero->GetPlayerSizeW() * 3)
		{
			hero->SetPlayerAnimX(0);
		}

		hero->SetPlayerPosY(hero->GetPlayerPos().y + hero->GetPlayerSpeed());

		IsCollision();

		if (!CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			hero->SetPlayerPosY(((int)(hero->GetPlayerPos().y / gameMap->GetTileSizeH())) * gameMap->GetTileSizeH() - 1);
		}
		else if (CanWalk(gameMap->GetCurrentRoom()->GetRoomMapTileID(hero->GetPlayerPos().x / gameMap->GetTileSizeW(), hero->GetPlayerPos().y / gameMap->GetTileSizeH())))
		{
			if (hero->GetPlayerPos().y > (gameMap->GetMapH() - (gameMap->GetTileSizeH() / 3)))
			{
				if (hero->GetPlayerPos().y > (gameMap->GetMapH() - (gameMap->GetTileSizeH() / 8)))
				{
					if (gameMap->GetCurrentRoom()->GetDoors()[3] == NULL)
					{
						gameMap->GetCurrentRoom()->GetDoors()[3] = new GameMapRoom(5, (int)gResolution);
						gameMap->GetCurrentRoom()->GetDoors()[3]->GetDoors()[0] = new GameMapRoom(99, (int)gResolution);
						gameMap->GetCurrentRoom()->GetDoors()[3]->GetDoors()[0] = gameMap->GetCurrentRoom();
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[3]);
						AddColliders();
						gameDownDoors->SetTileRectX(0);
						hero->SetPlayerPosY(gameMap->GetTileSizeH() / 2);
					}
					else
					{
						gameMap->SetCurrentRoom(gameMap->GetCurrentRoom()->GetDoors()[3]);
						gameDownDoors->SetTileRectX(0);
						hero->SetPlayerPosY(gameMap->GetTileSizeH() / 2);
					}
				}
			}
		}

		hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
	}
}

bool Game::CanWalk(int tileID)
{
	return Tile::IsWalkAvailable(tileID);
}

void Game::MenuDraw()
{
	SDL_Rect renderRect;
	renderRect.x = 0;
	renderRect.y = 0;

	if (gameMenu->GetTileRect().y < gameMap->GetMapH() * 3 && gameMenu->GetTileRect().y != 0)
	{
		SDL_BlitSurface(gameMenu->GetTileSurface(), &gameMenu->GetTileRect(), gameWindow->GetScreenSurface(), &renderRect);
		SDL_BlitSurface(gameMenuDot->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &gameMenuDot->GetTileRect());
		SDL_UpdateWindowSurface(gameWindow->GetWindow());
		return;
	}
	else if (gameMenu->GetTileRect().y >= gameMap->GetMapH() * 3)
	{
		SDL_Rect Render_TMP2;
		Render_TMP2.x = (gameMap->GetMapW()*0.26);
		Render_TMP2.y = (gameMap->GetMapH()*0.1);

		SDL_BlitSurface(gameMenu->GetTileSurface(), &gameMenu->GetTileRect(), gameWindow->GetScreenSurface(), &renderRect);
		SDL_BlitSurface(gameMenuDot->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &gameMenuDot->GetTileRect());
		SDL_BlitSurface(gameResolutions->GetTileSurface(), &gameResolutions->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP2);
		Render_TMP2.x = (gameMap->GetMapW()*0.29);
		Render_TMP2.y = (gameMap->GetMapH()*0.54);
		SDL_BlitSurface(fullscreenOnOff->GetTileSurface(), &fullscreenOnOff->GetTileRect(), gameWindow->GetScreenSurface(), &Render_TMP2);
		SDL_UpdateWindowSurface(gameWindow->GetWindow());
		return;
	}

	SDL_BlitSurface(gameMenu->GetTileSurface(), &gameMenu->GetTileRect(), gameWindow->GetScreenSurface(), &renderRect);
	SDL_UpdateWindowSurface(gameWindow->GetWindow());
}

void Game::GameEnding()
{
	SDL_Rect renderRect;
	renderRect.x = 0;
	renderRect.y = 0;

	SDL_BlitSurface(gameEnd->GetTileSurface(), &gameEnd->GetTileRect(), gameWindow->GetScreenSurface(), &renderRect);
	SDL_UpdateWindowSurface(gameWindow->GetWindow());

	bool userQuitRequest = false;

	SDL_Event event;

	SDL_Delay(1000);

	while (gameEnd->GetTileRect().y < (gameEnd->GetTileRect().h - gameMap->GetMapH()*1.05))
	{
		SDL_BlitSurface(gameEnd->GetTileSurface(), &gameEnd->GetTileRect(), gameWindow->GetScreenSurface(), &renderRect);
		SDL_UpdateWindowSurface(gameWindow->GetWindow());

		gameEnd->SetTileRectY(gameEnd->GetTileRect().y + 1);

		if (gameEnd->GetTileRect().y == (gameEnd->GetTileRect().h - gameMap->GetMapH()))
		{
			SDL_Delay(1000);
			break;
		}

		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				userQuitRequest = true;
				break;

			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
				{
					userQuitRequest = true;
					break;
				}
				case SDLK_q:
				{
					userQuitRequest = true;
					break;
				}
				}
				break;
			}

		}
		if (userQuitRequest)
		{
			break;
		}
	}

	if (!userQuitRequest)
	{
		SDL_Delay(2500);
	}

	isOpened = false;
}

void Game::MenuPauseDraw()
{
	SDL_Rect renderRect;
	renderRect.x = 0;
	renderRect.y = 0;
	renderRect.w = gameMap->GetMapW();
	renderRect.h = gameMap->GetMapH();

	Draw(gStatus);

	SDL_BlitSurface(gameMenuPause->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &renderRect);
	SDL_BlitSurface(gameMenuPauseDot->GetTileSurface(), NULL, gameWindow->GetScreenSurface(), &gameMenuPauseDot->GetTileRect());
	SDL_UpdateWindowSurface(gameWindow->GetWindow());
}

void Game::ChangeResolution(int state)
{
#pragma region Data Cleaning

	delete gameFloor;
	delete gameUpWall;
	delete gameLeftWall;
	delete gameRightWall;
	delete gameDownWall;
	delete firstCorner;
	delete secondCorner;
	delete thirdCorner;
	delete fourthCorner;
	delete gameUpDoors;
	delete gameLeftDoors;
	delete gameRightDoors;
	delete gameDownDoors;
	delete gameMenu;
	delete gameEnd;
	delete gameMenuDot;
	delete gameMenuPauseDot;
	delete gameMenuPause;
	delete gameResolutions;
	delete fullscreenOnOff;
	delete floorBlackHole;
	delete gameBackground;
	delete playerStats;
	delete textBackground;

#pragma endregion

	if (state == 0)
	{
		if (gResolution == RES_1024x768)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*0.783);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*0.783);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 0.783);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 0.783);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 0.783);
			hero->SetColliderW(hero->GetColliderW() * 0.783);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) * 0.783);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) * 0.783);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 0.783);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 0.783);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) * 0.783);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) * 0.783);
			}
		}
		else if (gResolution == RES_1280x960)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*0.621);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*0.621);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 0.621);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 0.621);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 0.621);
			hero->SetColliderW(hero->GetColliderW() * 0.621);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) * 0.62);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) * 0.62);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 0.621);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 0.621);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) * 0.621);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) * 0.621);
			}
		}
		else if (gResolution == RES_1440x1080)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*0.563);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*0.563);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 0.563);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 0.563);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 0.563);
			hero->SetColliderW(hero->GetColliderW() * 0.563);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						0.563);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						0.563);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 0.563);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 0.563);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		0.563);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	0.563);
			}
		}

		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 17);
		gResolution = RES_800x600;

		gameMap->SetTileSizeW(72);
		gameMap->SetTileSizeH(54);
		gameMap->SetTileAmount(11);
		gameMap->SetMapH(gameMap->GetTileSizeH() * gameMap->GetTileAmount());
		gameMap->SetMapW(gameMap->GetTileSizeW() * gameMap->GetTileAmount());
		gameMap->SetMapSize(gameMap->GetMapH() * gameMap->GetMapW());

		hero->EditSurface(PATH_PREFIX_800_X_600 + "pngHero.png", 39, 58);
		hero->SetPlayerSpeed(3);
		hero->SetPlayerAnimationSpeed(1);
		menuDotW = 60;
		menuDotH = 54;

		money->EditSurface(PATH_PREFIX_800_X_600 + "pngMoney.png", 32, 32);
		hole_1->EditSurface(PATH_PREFIX_800_X_600 + "Hole_1.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_2->EditSurface(PATH_PREFIX_800_X_600 + "Hole_2.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_3->EditSurface(PATH_PREFIX_800_X_600 + "Hole_3.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_4->EditSurface(PATH_PREFIX_800_X_600 + "Hole_4.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_5->EditSurface(PATH_PREFIX_800_X_600 + "Hole_5.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_6->EditSurface(PATH_PREFIX_800_X_600 + "Hole_6.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_7->EditSurface(PATH_PREFIX_800_X_600 + "Hole_7.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_8->EditSurface(PATH_PREFIX_800_X_600 + "Hole_8.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_9->EditSurface(PATH_PREFIX_800_X_600 + "Hole_9.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_10->EditSurface(PATH_PREFIX_800_X_600 + "Hole_10.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_11->EditSurface(PATH_PREFIX_800_X_600 + "Hole_11.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_12->EditSurface(PATH_PREFIX_800_X_600 + "Hole_12.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_13->EditSurface(PATH_PREFIX_800_X_600 + "pngHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_14->EditSurface(PATH_PREFIX_800_X_600 + "pngBigHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());


		gameFloor = new Tile(Tile::Floor, true, PATH_PREFIX_800_X_600 + "bmpFloor2.bmp");
		gameUpWall = new Tile(Tile::UpperWall, false, PATH_PREFIX_800_X_600 + "bmpUpsideWall.bmp");
		gameLeftWall = new Tile(Tile::LeftWall, false, PATH_PREFIX_800_X_600 + "bmpLeftsideWall.bmp");
		gameRightWall = new Tile(Tile::RightWall, false, PATH_PREFIX_800_X_600 + "bmpRightsideWall.bmp");
		gameDownWall = new Tile(Tile::BottomWall, false, PATH_PREFIX_800_X_600 + "bmpDownsideWall.bmp");
		firstCorner = new Tile(Tile::UpperCornerL, false, PATH_PREFIX_800_X_600 + "FirstCorner.bmp");
		secondCorner = new Tile(Tile::UpperCornerR, false, PATH_PREFIX_800_X_600 + "SecondCorner.bmp");
		thirdCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_800_X_600 + "ThirdCorner.bmp");
		fourthCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_800_X_600 + "FourthCorner.bmp");
		gameUpDoors = new AnimatedTile(AnimatedTile::UpperDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameLeftDoors = new AnimatedTile(AnimatedTile::LeftDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameRightDoors = new AnimatedTile(AnimatedTile::RightDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameDownDoors = new AnimatedTile(AnimatedTile::BottomDoors, true, PATH_PREFIX_800_X_600 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameMenu = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_800_X_600 + "pngGameMenu.png", 0, gameMap->GetMapH() * 2, gameMap->GetMapW(), gameMap->GetMapH());
		gameEnd = new AnimatedTile(AnimatedTile::Ending, false, PATH_PREFIX_800_X_600 + "pngGameEnding.png", 0, 0, gameMap->GetMapW(), 1270);
		gameMenuDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_800_X_600 + "pngGameMenuDot.png", (gameMap->GetMapW()*0.32), (gameMap->GetMapH()*0.1), menuDotW, menuDotH);
		gameMenuPauseDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_800_X_600 + "pngGameMenuPauseDot.png", 0, 0, menuDotW, menuDotH);
		gameMenuPause = new AnimatedTile(AnimatedTile::Pause, false, PATH_PREFIX_800_X_600 + "pngGameMenuPause.png", 0, 0, gameMap->GetMapW(), gameMap->GetMapH());
		gameResolutions = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_800_X_600 + "pngGameResolutions.png", 0, 0, 408, (360 / 4));
		fullscreenOnOff = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_800_X_600 + "pngFulscreenOnOff.png", 0, 75, 324, (150 / 2));
		floorBlackHole = new Tile(Tile::Hole, true, PATH_PREFIX_800_X_600 + "bmpBlackHole.bmp");
		gameBackground = new Tile(Tile::Floor, false, PATH_PREFIX_800_X_600 + "GameBackground.bmp");
		playerStats = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_800_X_600 + "pngStats.png", 0, 0, 72, 27);
		textBackground = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_800_X_600 + "pngStatsTextBackground.png", 0, 0, 72, 27);

		delete gameWindow;
		gameWindow = new GameWindow("Rogalik", 792, 594, false);
		gameWindow->SetScreenSurface(SDL_GetWindowSurface(gameWindow->GetWindow()));
		return;
	}
	else if (state == 1)
	{
		if (gResolution == RES_800x600)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*1.278);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*1.278);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 1.278);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 1.278);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 1.278);
			hero->SetColliderW(hero->GetColliderW() * 1.278);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						1.278);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						1.278);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 1.278);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 1.278);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		1.278);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	1.278);
			}
		}
		else if (gResolution == RES_1280x960)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*0.794);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*0.794);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 0.794);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 0.794);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 0.794);
			hero->SetColliderW(hero->GetColliderW() * 0.794);
			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						0.794);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						0.794);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 0.794);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 0.794);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		0.794);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	0.794);
			}
		}
		else if (gResolution == RES_1440x1080)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*0.719);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*0.719);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 0.719);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 0.719);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 0.719);
			hero->SetColliderW(hero->GetColliderW() * 0.719);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						0.719);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						0.719);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 0.719);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 0.719);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		0.719);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	0.719);
			}
		}

		gResolution = RES_1024x768;
		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 20);


		gameMap->SetTileSizeW(92);
		gameMap->SetTileSizeH(69);
		gameMap->SetTileAmount(11);
		gameMap->SetMapH(gameMap->GetTileSizeH() * gameMap->GetTileAmount());
		gameMap->SetMapW(gameMap->GetTileSizeW() * gameMap->GetTileAmount());
		gameMap->SetMapSize(gameMap->GetMapH() * gameMap->GetMapW());
		hero->EditSurface(PATH_PREFIX_1024_X_768 + "pngHero.png", 49, 74);
		hero->SetPlayerSpeed(4);
		hero->SetPlayerAnimationSpeed(1);
		menuDotW = 77;
		menuDotH = 69;

		money->EditSurface(PATH_PREFIX_1024_X_768 + "pngMoney.png", 49, 37);
		hole_1->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_1.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_2->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_2.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_3->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_3.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_4->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_4.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_5->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_5.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_6->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_6.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_7->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_7.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_8->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_8.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_9->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_9.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_10->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_10.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_11->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_11.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_12->EditSurface(PATH_PREFIX_1024_X_768 + "Hole_12.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_13->EditSurface(PATH_PREFIX_1024_X_768 + "pngHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_14->EditSurface(PATH_PREFIX_1024_X_768 + "pngBigHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());

		gameFloor = new Tile(Tile::Floor, true, PATH_PREFIX_1024_X_768 + "bmpFloor2.bmp");
		gameUpWall = new Tile(Tile::UpperWall, false, PATH_PREFIX_1024_X_768 + "bmpUpsideWall.bmp");
		gameLeftWall = new Tile(Tile::LeftWall, false, PATH_PREFIX_1024_X_768 + "bmpLeftsideWall.bmp");
		gameRightWall = new Tile(Tile::RightWall, false, PATH_PREFIX_1024_X_768 + "bmpRightsideWall.bmp");
		gameDownWall = new Tile(Tile::BottomWall, false, PATH_PREFIX_1024_X_768 + "bmpDownsideWall.bmp");
		firstCorner = new Tile(Tile::UpperCornerL, false, PATH_PREFIX_1024_X_768 + "FirstCorner.bmp");
		secondCorner = new Tile(Tile::UpperCornerR, false, PATH_PREFIX_1024_X_768 + "SecondCorner.bmp");
		thirdCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_1024_X_768 + "ThirdCorner.bmp");
		fourthCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_1024_X_768 + "FourthCorner.bmp");
		gameUpDoors = new AnimatedTile(AnimatedTile::UpperDoors, true, PATH_PREFIX_1024_X_768 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameLeftDoors = new AnimatedTile(AnimatedTile::LeftDoors, true, PATH_PREFIX_1024_X_768 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameRightDoors = new AnimatedTile(AnimatedTile::RightDoors, true, PATH_PREFIX_1024_X_768 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameDownDoors = new AnimatedTile(AnimatedTile::BottomDoors, true, PATH_PREFIX_1024_X_768 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameMenu = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1024_X_768 + "pngGameMenu.png", 0, gameMap->GetMapH() * 2, gameMap->GetMapW(), gameMap->GetMapH());
		gameEnd = new AnimatedTile(AnimatedTile::Ending, false, PATH_PREFIX_1024_X_768 + "pngGameEnding.png", 0, 0, gameMap->GetMapW(), 1623);
		gameMenuDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_1024_X_768 + "pngGameMenuDot.png", (gameMap->GetMapW()*0.32), (gameMap->GetMapH()*0.1), menuDotW, menuDotH);
		gameMenuPauseDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_1024_X_768 + "pngGameMenuPauseDot.png", 0, 0, menuDotW, menuDotH);
		gameMenuPause = new AnimatedTile(AnimatedTile::Pause, false, PATH_PREFIX_1024_X_768 + "pngGameMenuPause.png", 0, 0, gameMap->GetMapW(), gameMap->GetMapH());
		gameResolutions = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1024_X_768 + "pngGameResolutions.png", 0, 115, 521, (460 / 4));
		fullscreenOnOff = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1024_X_768 + "pngFulscreenOnOff.png", 0, (190 / 2), 415, (190 / 2));
		floorBlackHole = new Tile(Tile::Hole, true, PATH_PREFIX_1024_X_768 + "bmpBlackHole.bmp");
		gameBackground = new Tile(Tile::Floor, false, PATH_PREFIX_1024_X_768 + "GameBackground.bmp");
		playerStats = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_1024_X_768 + "pngStats.png", 0, 0, 92, 34);
		textBackground = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_1024_X_768 + "pngStatsTextBackground.png", 0, 0, 92, 34);

		delete gameWindow;
		gameWindow = new GameWindow("Rogalik", 1012, 759, false);
		gameWindow->SetScreenSurface(SDL_GetWindowSurface(gameWindow->GetWindow()));
		return;
	}
	else if (state == 2)
	{
		if (gResolution == RES_800x600)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*1.612);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*1.612);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 1.612);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 1.612);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 1.612);
			hero->SetColliderW(hero->GetColliderW() * 1.612);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						1.612);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						1.612);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 1.612);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 1.612);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		1.612);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	1.612);
			}
		}
		else if (gResolution == RES_1024x768)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*1.261);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*1.261);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 1.261);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 1.261);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 1.261);
			hero->SetColliderW(hero->GetColliderW() * 1.261);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						1.261);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						1.261);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 1.261);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 1.261);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		1.261);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	1.261);
			}
		}
		else if (gResolution == RES_1440x1080)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*0.907);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*0.907);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 0.907);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 0.907);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 0.907);
			hero->SetColliderW(hero->GetColliderW() * 0.907);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						0.907);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						0.907);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 0.907);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 0.907);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		0.907);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	0.907);
			}
		}

		gResolution = RES_1280x960;
		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 23);

		gameMap->SetTileSizeW(116);
		gameMap->SetTileSizeH(87);
		gameMap->SetTileAmount(11);
		gameMap->SetMapH(gameMap->GetTileSizeH() * gameMap->GetTileAmount());
		gameMap->SetMapW(gameMap->GetTileSizeW() * gameMap->GetTileAmount());
		gameMap->SetMapSize(gameMap->GetMapH() * gameMap->GetMapW());

		hero->EditSurface(PATH_PREFIX_1280_X_960 + "pngHero.png", 62, 93);


		hero->SetPlayerSpeed(5);
		hero->SetPlayerAnimationSpeed(1);
		menuDotW = 97;
		menuDotH = 87;

		money->EditSurface(PATH_PREFIX_1280_X_960 + "pngMoney.png", 62, 46);
		hole_1->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_1.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_2->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_2.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_3->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_3.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_4->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_4.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_5->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_5.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_6->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_6.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_7->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_7.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_8->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_8.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_9->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_9.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_10->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_10.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_11->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_11.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_12->EditSurface(PATH_PREFIX_1280_X_960 + "Hole_12.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_13->EditSurface(PATH_PREFIX_1280_X_960 + "pngHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_14->EditSurface(PATH_PREFIX_1280_X_960 + "pngBigHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());


		gameFloor = new Tile(Tile::Floor, true, PATH_PREFIX_1280_X_960 + "bmpFloor2.bmp");
		gameUpWall = new Tile(Tile::UpperWall, false, PATH_PREFIX_1280_X_960 + "bmpUpsideWall.bmp");
		gameLeftWall = new Tile(Tile::LeftWall, false, PATH_PREFIX_1280_X_960 + "bmpLeftsideWall.bmp");
		gameRightWall = new Tile(Tile::RightWall, false, PATH_PREFIX_1280_X_960 + "bmpRightsideWall.bmp");
		gameDownWall = new Tile(Tile::BottomWall, false, PATH_PREFIX_1280_X_960 + "bmpDownsideWall.bmp");
		firstCorner = new Tile(Tile::UpperCornerL, false, PATH_PREFIX_1280_X_960 + "FirstCorner.bmp");
		secondCorner = new Tile(Tile::UpperCornerR, false, PATH_PREFIX_1280_X_960 + "SecondCorner.bmp");
		thirdCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_1280_X_960 + "ThirdCorner.bmp");
		fourthCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_1280_X_960 + "FourthCorner.bmp");
		gameUpDoors = new AnimatedTile(AnimatedTile::UpperDoors, true, PATH_PREFIX_1280_X_960 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameLeftDoors = new AnimatedTile(AnimatedTile::LeftDoors, true, PATH_PREFIX_1280_X_960 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameRightDoors = new AnimatedTile(AnimatedTile::RightDoors, true, PATH_PREFIX_1280_X_960 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameDownDoors = new AnimatedTile(AnimatedTile::BottomDoors, true, PATH_PREFIX_1280_X_960 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameMenu = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1280_X_960 + "pngGameMenu.png", 0, gameMap->GetMapH() * 2, gameMap->GetMapW(), gameMap->GetMapH());
		gameEnd = new AnimatedTile(AnimatedTile::Ending, false, PATH_PREFIX_1280_X_960 + "pngGameEnding.png", 0, 0, gameMap->GetMapW(), 2046);
		gameMenuDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_1280_X_960 + "pngGameMenuDot.png", (gameMap->GetMapW()*0.32), (gameMap->GetMapH()*0.1), menuDotW, menuDotH);
		gameMenuPauseDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_1280_X_960 + "pngGameMenuPauseDot.png", 0, 0, menuDotW, menuDotH);
		gameMenuPause = new AnimatedTile(AnimatedTile::Pause, false, PATH_PREFIX_1280_X_960 + "pngGameMenuPause.png", 0, 0, gameMap->GetMapW(), gameMap->GetMapH());
		gameResolutions = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1280_X_960 + "pngGameResolutions.png", 0, 290, 657, (580 / 4));
		fullscreenOnOff = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1280_X_960 + "pngFulscreenOnOff.png", 0, (238 / 2), 524, (238 / 2));
		floorBlackHole = new Tile(Tile::Hole, true, PATH_PREFIX_1280_X_960 + "bmpBlackHole.bmp");
		gameBackground = new Tile(Tile::Floor, false, PATH_PREFIX_1280_X_960 + "GameBackground.bmp");
		playerStats = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_1280_X_960 + "pngStats.png", 0, 0, 116, 43);
		textBackground = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_1280_X_960 + "pngStatsTextBackground.png", 0, 0, 116, 43);

		delete gameWindow;
		gameWindow = new GameWindow("Rogalik", 1276, 957, false);
		gameWindow->SetScreenSurface(SDL_GetWindowSurface(gameWindow->GetWindow()));
		return;
	}
	else if (state == 3)
	{
		if (gResolution == RES_800x600)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*1.778);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*1.778);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 1.778);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 1.778);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 1.778);
			hero->SetColliderW(hero->GetColliderW() * 1.778);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						1.778);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						1.778);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 1.778);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 1.778);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		1.778);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	1.778);
			}
		}
		else if (gResolution == RES_1024x768)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*1.392);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*1.392);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 1.392);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 1.392);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 1.392);
			hero->SetColliderW(hero->GetColliderW() * 1.392);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						1.392);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						1.392);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 1.392);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 1.392);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		1.392);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	1.392);
			}
		}
		else if (gResolution == RES_1280x960)
		{
			hero->SetPlayerPosX(hero->GetPlayerPos().x*1.104);
			hero->SetPlayerPosY(hero->GetPlayerPos().y*1.104);
			hero->SetColliderPosX_TMP(hero->GetColliderPosX_TMP() * 1.104);
			hero->SetColliderPosY_TMP(hero->GetColliderPosY_TMP() * 1.104);
			hero->SetPosX(hero->GetPlayerPos().x + hero->GetColliderPosX_TMP());
			hero->SetPosY(hero->GetPlayerPos().y + hero->GetColliderPosY_TMP());
			hero->SetColliderH(hero->GetColliderH() * 1.104);
			hero->SetColliderW(hero->GetColliderW() * 1.104);

			for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
			{
				gameMap->GetCurrentRoom()->SetRoomObjectPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) *						1.104);
				gameMap->GetCurrentRoom()->SetRoomObjectPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) *						1.104);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15 * 1.104);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48 * 1.104);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderWidth(i) *		1.104);
				gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, gameMap->GetCurrentRoom()->GetRoomObjectColliderHeight(i) *	1.104);
			}
		}

		gResolution = RES_1440x1080;
		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 26);

		gameMap->SetTileSizeW(128);
		gameMap->SetTileSizeH(96);
		gameMap->SetTileAmount(11);
		gameMap->SetMapH(gameMap->GetTileSizeH() * gameMap->GetTileAmount());
		gameMap->SetMapW(gameMap->GetTileSizeW() * gameMap->GetTileAmount());
		gameMap->SetMapSize(gameMap->GetMapH() * gameMap->GetMapW());
		hero->EditSurface(PATH_PREFIX_1440_X_1080 + "pngHero.png", 68, 102);

		hero->SetPlayerSpeed(6);
		hero->SetPlayerAnimationSpeed(1);
		menuDotW = 106;
		menuDotH = 96;

		money->EditSurface(PATH_PREFIX_1440_X_1080 + "pngMoney.png", 68, 51);
		hole_1->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_1.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_2->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_2.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_3->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_3.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_4->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_4.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_5->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_5.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_6->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_6.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_7->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_7.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_8->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_8.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_9->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_9.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_10->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_10.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_11->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_11.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_12->EditSurface(PATH_PREFIX_1440_X_1080 + "Hole_12.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_13->EditSurface(PATH_PREFIX_1440_X_1080 + "pngHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		hole_14->EditSurface(PATH_PREFIX_1440_X_1080 + "pngBigHole.png", gameMap->GetTileSizeW(), gameMap->GetTileSizeH());

		gameFloor = new Tile(Tile::Floor, true, PATH_PREFIX_1440_X_1080 + "bmpFloor2.bmp");
		gameUpWall = new Tile(Tile::UpperWall, false, PATH_PREFIX_1440_X_1080 + "bmpUpsideWall.bmp");
		gameLeftWall = new Tile(Tile::LeftWall, false, PATH_PREFIX_1440_X_1080 + "bmpLeftsideWall.bmp");
		gameRightWall = new Tile(Tile::RightWall, false, PATH_PREFIX_1440_X_1080 + "bmpRightsideWall.bmp");
		gameDownWall = new Tile(Tile::BottomWall, false, PATH_PREFIX_1440_X_1080 + "bmpDownsideWall.bmp");
		firstCorner = new Tile(Tile::UpperCornerL, false, PATH_PREFIX_1440_X_1080 + "FirstCorner.bmp");
		secondCorner = new Tile(Tile::UpperCornerR, false, PATH_PREFIX_1440_X_1080 + "SecondCorner.bmp");
		thirdCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_1440_X_1080 + "ThirdCorner.bmp");
		fourthCorner = new Tile(Tile::BottomCornerL, false, PATH_PREFIX_1440_X_1080 + "FourthCorner.bmp");
		gameUpDoors = new AnimatedTile(AnimatedTile::UpperDoors, true, PATH_PREFIX_1440_X_1080 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameLeftDoors = new AnimatedTile(AnimatedTile::LeftDoors, true, PATH_PREFIX_1440_X_1080 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameRightDoors = new AnimatedTile(AnimatedTile::RightDoors, true, PATH_PREFIX_1440_X_1080 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameDownDoors = new AnimatedTile(AnimatedTile::BottomDoors, true, PATH_PREFIX_1440_X_1080 + "pngDoors.png", 0, 0, gameMap->GetTileSizeW(), gameMap->GetTileSizeH());
		gameMenu = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1440_X_1080 + "pngGameMenu.png", 0, gameMap->GetMapH() * 2, gameMap->GetMapW(), gameMap->GetMapH());
		gameEnd = new AnimatedTile(AnimatedTile::Ending, false, PATH_PREFIX_1440_X_1080 + "pngGameEnding.png", 0, 0, gameMap->GetMapW(), 2257);
		gameMenuDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_1440_X_1080 + "pngGameMenuDot.png", (gameMap->GetMapW()*0.32), (gameMap->GetMapH()*0.1), menuDotW, menuDotH);
		gameMenuPauseDot = new AnimatedTile(AnimatedTile::MenuDot, false, PATH_PREFIX_1440_X_1080 + "pngGameMenuPauseDot.png", 0, 0, menuDotW, menuDotH);
		gameMenuPause = new AnimatedTile(AnimatedTile::Pause, false, PATH_PREFIX_1440_X_1080 + "pngGameMenuPause.png", 0, 0, gameMap->GetMapW(), gameMap->GetMapH());
		gameResolutions = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1440_X_1080 + "pngGameResolutions.png", 0, 480, 725, (640 / 4));
		fullscreenOnOff = new AnimatedTile(AnimatedTile::Menu, false, PATH_PREFIX_1440_X_1080 + "pngFulscreenOnOff.png", 0, (262 / 2), 578, (262 / 2));
		floorBlackHole = new Tile(Tile::Hole, true, PATH_PREFIX_1440_X_1080 + "bmpBlackHole.bmp");
		gameBackground = new Tile(Tile::Floor, false, PATH_PREFIX_1440_X_1080 + "GameBackground.bmp");
		playerStats = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_1440_X_1080 + "pngStats.png", 0, 0, 128, 48);
		textBackground = new AnimatedTile(Tile::Stats, false, PATH_PREFIX_1440_X_1080 + "pngStatsTextBackground.png", 0, 0, 128, 48);

		delete gameWindow;
		gameWindow = new GameWindow("Rogalik", 1400, 1050, false);
		gameWindow->SetScreenSurface(SDL_GetWindowSurface(gameWindow->GetWindow()));
		return;
	}
}

void Game::AddColliders()
{
	for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
	{
		if (gameMap->GetCurrentRoom()->GetRoomObjectID(i) == 100 || gameMap->GetCurrentRoom()->GetRoomObjectID(i) == 101)
		{
			gameMap->GetCurrentRoom()->SetRoomObjectColliderPosX(i, gameMap->GetCurrentRoom()->GetRoomObjectPosX(i) + 15);
			gameMap->GetCurrentRoom()->SetRoomObjectColliderPosY(i, gameMap->GetCurrentRoom()->GetRoomObjectPosY(i) + 48);
			gameMap->GetCurrentRoom()->SetRoomObjectColliderWidth(i, 20);
			gameMap->GetCurrentRoom()->SetRoomObjectColliderHeight(i, 30);
		}
	}
}

bool Game::IsCollision()
{
	int heroCollPosX = hero->GetPosX();
	int heroCollPosY = hero->GetPosY();
	int heroCollW = hero->GetColliderW();
	int heroCollH = hero->GetColliderH();
	GameMapRoom* room = gameMap->GetCurrentRoom();

	for (int i = 0; i < gameMap->GetCurrentRoom()->GetRoomObjectsSize(); i++)
	{
		int roomObjectCollPosX = room->GetRoomObjectColliderPosX(i);
		int roomObjectCollPosY = room->GetRoomObjectColliderPosY(i);

		if (heroCollPosX > roomObjectCollPosX + room->GetRoomObjectColliderWidth(i) ||
			heroCollPosX + heroCollW < roomObjectCollPosX ||
			heroCollPosY > roomObjectCollPosY + room->GetRoomObjectColliderHeight(i) ||
			heroCollPosY + heroCollH < roomObjectCollPosY)
		{
			continue;
		}
		else
		{
			if (gameMap->GetCurrentRoom()->GetRoomObjectID(i) == AnimatedGameObject::e_money)
			{
				Mix_PlayChannel(-1, gCoin, 0);
				hero->SetPlayerMoney(hero->GetPlayerMoney() + 1);
				gameMap->GetCurrentRoom()->EraseRoomObject(i);
				return true;
			}
			else if (gameMap->GetCurrentRoom()->GetRoomObjectID(i) == AnimatedGameObject::e_heart)
			{
				Mix_PlayChannel(-1, gHeart, 1);
				hero->SetPlayerHealth(hero->GetPlayerHealth() + 1);
				gameMap->GetCurrentRoom()->EraseRoomObject(i);
				return true;
			}
		}
	}

	return false;
}