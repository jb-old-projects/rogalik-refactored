/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#include "Tile.h"

class AnimatedTile : public Tile
{
public:
	AnimatedTile(TileType type, bool isWalkable, std::string bmpName, int x, int y, int w, int h);

	SDL_Rect GetTileRect();

	void SetTileRectX(int x);
	void SetTileRectY(int y);

private:
	SDL_Rect tileRect;
};