/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#pragma comment (lib, "SDL2_image")
#include <string>
#include <SDL2\SDL_image.h>

class Player
{
public:

	Player(std::string bmpName, int playerPosX, int playerPosY, int playerSizeW, int playerSizeH);
	~Player();
	void EditSurface(std::string bmpName, int playerSizeW, int playerSizeH);	// Metoda zmieniaj�ca surface w momencie zmiany rozdzielczo�ci

#pragma region Getters

	SDL_Surface* GetTileSurface();
	SDL_Rect GetPlayerPos();
	SDL_Rect GetPlayerAnim();
	SDL_Rect GetPlayerRenderRect();
	int GetPlayerHealth();
	int GetPlayerSpeed();
	int GetPlayerMoney();
	int GetPlayerSizeW();
	int GetPlayerSizeH();
	int GetColliderPosX_TMP();
	int GetColliderPosY_TMP();
	int GetColliderW();
	int GetColliderH();
	int GetPlayerAnimationSpeed();
	int GetPosX();
	int GetPosY();

#pragma endregion

#pragma region Setters

	void SetColliderPosX_TMP(int x);
	void SetColliderPosY_TMP(int y);
	void SetColliderW(int w);
	void SetColliderH(int h);
	void SetPlayerPosX(int x);
	void SetPlayerPosY(int y);
	void SetPlayerRenderRectX(int x);
	void SetPlayerRenderRectY(int y);
	void SetPlayerSpeed(int s);
	void SetPlayerMoney(int m);
	void SetPlayerHealth(int h);
	void SetPlayerAnimX(int x);
	void SetPlayerAnimY(int y);
	void SetPlayerAnimationSpeed(int s);
	void SetPosX(int x);
	void SetPosY(int y);

#pragma endregion

private:

#pragma region Variables

	SDL_RWops* rwop;
	SDL_Surface* tileSurface = NULL;
	SDL_Rect playerPos;
	SDL_Rect playerAnim;
	SDL_Rect playerRenderRect;

	int posX;
	int posY;
	int playerAnimationSpeed = 1;

	int playerHealth;
	int playerSpeed;
	int playerMoney;

	int playerSizeW;
	int playerSizeH;

	int colliderW;
	int colliderH;
	int colliderPosX_TMP;
	int colliderPosY_TMP;

#pragma endregion

};
