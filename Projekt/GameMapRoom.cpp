/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "GameMapRoom.h"
#include "GameMap.h"

GameMapRoom::GameMapRoom(int id, int gStatus)
{
	GameMap::SetRoomAmount(GameMap::GetRoomAmount() + 1);

	int tileAmount = 11;
	for (int i = 0; i < tileAmount; i++)
	{
		for (int j = 0; j < tileAmount; j++)
		{
			roomMap[i][j] = 99;;
		}
	}

	srand(time(0));
	int TMP;
	int TMP2 = 1;
	int TMPtab[4]{ -1 };

	if (GameMap::GetRoomAmount() > 15)
	{
		if (doors[0] == NULL && doors[1] == NULL && doors[2] == NULL && doors[3] == NULL)
		{
			TMP = rand() % 1001;
			if (TMP <= 800)
				TMP2++;
		}
	}
	else
		TMP2++;
	TMP = rand() % 1001;
	if (TMP <= 690)
		TMP2++;
	TMP = rand() % 1001;
	if (TMP <= 450)
		TMP2++;

	for (int i = 0; i < tileAmount; i++)
	{
		for (int j = 0; j < tileAmount; j++)
		{
			if (id == 5 && i == 0 && j == 5)
			{
				roomMap[j][i] = 5;
				TMP2--;
				TMPtab[0] = 1;
			}
			else if (id == 6 && i == 5 && j == 0)
			{
				roomMap[j][i] = 6;
				TMP2--;
				TMPtab[1] = 1;
			}
			else if (id == 7 && i == 5 && j == 10)
			{
				roomMap[j][i] = 7;
				TMP2--;
				TMPtab[2] = 1;
			}
			else if (id == 8 && i == 10 && j == 5)
			{
				roomMap[j][i] = 8;
				TMP2--;
				TMPtab[3] = 1;
			}
		}
	}

	TMP = rand() % 4;
	TMPtab[TMP] = 1;

	for (int i = 0; i < TMP2; i++)
	{
		TMP = rand() % 4;
		TMPtab[TMP] = 1;
	}

	TMP = rand() % 1001;

	if (TMP <= 690 && GameMap::GetRoomAmount() > 1)
		GenerateHoles(TMPtab, gStatus);


	for (int i = 0; i < tileAmount; i++)
	{
		for (int j = 0; j < tileAmount; j++)
		{
			if (roomMap[j][i] == 99)
			{
				if (i == 0)
				{
					if (j == 5 && TMP2 > 0 && TMPtab[0] == 1)
					{
						roomMap[j][i] = 5;
						TMP2--;
					}
					else if (j == 0)
						roomMap[j][i] = 9;
					else if (j == 10)
						roomMap[j][i] = 10;
					else
						roomMap[j][i] = 1;
				}
				else if (i == 10)
				{
					if (j == 5 && TMP2 > 0 && TMPtab[3] == 1)
					{
						roomMap[j][i] = 8;
						TMP2--;
					}
					else if (j == 0)
						roomMap[j][i] = 11;
					else if (j == 10)
						roomMap[j][i] = 12;
					else
						roomMap[j][i] = 4;
				}
				else if (j == 0)
				{
					if (i == 5 && TMP2 > 0 && TMPtab[1] == 1)
					{
						roomMap[j][i] = 6;
						TMP2--;
					}
					else
						roomMap[j][i] = 2;
				}
				else if (j == 10)
				{
					if (i == 5 && TMP2 > 0 && TMPtab[2] == 1)
					{
						roomMap[j][i] = 7;
						TMP2--;
					}
					else
						roomMap[j][i] = 3;
				}
				else
					roomMap[j][i] = 0;
			}
		}
	}

	TMP = rand() % 1001;

	if (TMP <= 751)
		GenerateMoney(gStatus);

	TMP = rand() & 1001;

	if (TMP <= 151)
		GenerateHearts(gStatus);
}

GameMapRoom::~GameMapRoom()
{
	if (doors[0] != NULL)
	{
		doors[0]->doors[3] = NULL;
		delete doors[0];
		doors[0] = NULL;
	}

	if (doors[1] != NULL)
	{
		doors[1]->doors[2] = NULL;
		delete doors[1];
		doors[1] = NULL;
	}

	if (doors[2] != NULL)
	{
		doors[2]->doors[1] = NULL;
		delete doors[2];
		doors[2] = NULL;
	}

	if (doors[3] != NULL)
	{
		doors[3]->doors[0] = NULL;
		delete doors[3];
		doors[3] = NULL;

	}

	roomObjects.clear();
}

void GameMapRoom::GenerateHoles(int state[4], int gStatus)
{
	srand(time(0));
	int tileAmount = 11;
	int Rand_TMP;
	int EasterEggRand;

	EasterEggRand = rand() % 1001;

	Rand_TMP = 1 + rand() % 8;

	int w, h;

	if (gStatus == 0)
	{
		w = 72;
		h = 54;
	}
	else if (gStatus == 1)
	{
		w = 92;
		h = 69;
	}
	else if (gStatus == 2)
	{
		w = 116;
		h = 87;
	}
	else if (gStatus == 3)
	{
		w = 128;
		h = 96;
	}

	if (Rand_TMP == 8)
	{
		if (!(EasterEggRand <= 101))
			Rand_TMP = 5;
	}

	switch (Rand_TMP)
	{
	case 1:
	{
		roomMap[2][2] = 18;
		RoomObjectsInfo TMP2(2 * w, 2 * h, 114);
		roomObjects.push_back(TMP2);

		roomMap[5][2] = 18;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 2 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[8][2] = 18;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 2 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[2][5] = 18;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[8][5] = 18;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[2][8] = 18;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[5][8] = 18;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[8][8] = 18;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		return;
		break;
	}
	case 2:
	{

		roomMap[5][2] = 18;
		RoomObjectsInfo TMP2(5 * w, 2 * h, 114);
		roomObjects.push_back(TMP2);

		roomMap[2][5] = 18;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[8][5] = 18;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[5][8] = 18;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);


		return;
		break;
	}
	case 3:
	{
		roomMap[2][2] = 18;
		RoomObjectsInfo TMP2(2 * w, 2 * h, 114);
		roomObjects.push_back(TMP2);

		roomMap[8][2] = 18;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 2 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[2][8] = 18;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);

		roomMap[8][8] = 18;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 114;
		roomObjects.push_back(TMP2);


		return;
		break;
	}
	case 4:
	{
		roomMap[5][2] = 20;
		RoomObjectsInfo TMP2(5 * w, 2 * h, 102);
		roomObjects.push_back(TMP2);

		roomMap[4][3] = 24;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 106;
		roomObjects.push_back(TMP2);

		roomMap[5][3] = 32;

		roomMap[6][3] = 25;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 107;
		roomObjects.push_back(TMP2);

		roomMap[3][4] = 24;
		TMP2.roomObjectPosX = 3 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 106;
		roomObjects.push_back(TMP2);

		roomMap[4][4] = 32;

		roomMap[5][4] = 32;

		roomMap[6][4] = 32;


		roomMap[7][4] = 25;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 107;
		roomObjects.push_back(TMP2);

		roomMap[2][5] = 21;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 103;
		roomObjects.push_back(TMP2);

		roomMap[3][5] = 32;

		roomMap[4][5] = 32;

		roomMap[5][5] = 32;

		roomMap[6][5] = 32;

		roomMap[7][5] = 32;

		roomMap[8][5] = 22;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 104;
		roomObjects.push_back(TMP2);

		roomMap[3][6] = 26;
		TMP2.roomObjectPosX = 3 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 108;
		roomObjects.push_back(TMP2);

		roomMap[4][6] = 32;

		roomMap[5][6] = 32;

		roomMap[6][6] = 32;

		roomMap[7][6] = 27;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);

		roomMap[4][7] = 26;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 108;
		roomObjects.push_back(TMP2);

		roomMap[5][7] = 32;

		roomMap[6][7] = 27;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);

		roomMap[5][8] = 23;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 105;
		roomObjects.push_back(TMP2);

		return;
		break;
	}
	case 5:
	{
		RoomObjectsInfo TMP2(5 * w, 2 * h, 102);
		if (state[0] != 0)
		{
			roomMap[5][1] = 0;

			roomMap[5][2] = 0;

			roomMap[5][3] = 0;

			roomMap[5][4] = 0;

			roomMap[5][5] = 0;

			roomMap[4][1] = 30;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 1 * h;
			TMP2.roomObjectID = 112;
			roomObjects.push_back(TMP2);

			roomMap[4][2] = 30;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 2 * h;
			TMP2.roomObjectID = 112;
			roomObjects.push_back(TMP2);

			roomMap[4][3] = 30;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 3 * h;
			TMP2.roomObjectID = 112;
			roomObjects.push_back(TMP2);

			roomMap[4][4] = 30;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 112;
			roomObjects.push_back(TMP2);

			if (state[1] == 0)
			{
				roomMap[4][5] = 30;

				TMP2.roomObjectPosX = 4 * w;
				TMP2.roomObjectPosY = 5 * h;
				TMP2.roomObjectID = 112;
				roomObjects.push_back(TMP2);

			}


			roomMap[6][1] = 29;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 1 * h;
			TMP2.roomObjectID = 111;
			roomObjects.push_back(TMP2);

			roomMap[6][2] = 29;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 2 * h;
			TMP2.roomObjectID = 111;
			roomObjects.push_back(TMP2);

			roomMap[6][3] = 29;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 3 * h;
			TMP2.roomObjectID = 111;
			roomObjects.push_back(TMP2);

			roomMap[6][4] = 29;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 111;
			roomObjects.push_back(TMP2);

			if (state[2] == 0)
			{
				roomMap[6][5] = 29;

				TMP2.roomObjectPosX = 6 * w;
				TMP2.roomObjectPosY = 5 * h;
				TMP2.roomObjectID = 111;
				roomObjects.push_back(TMP2);
			}
		}

		if (state[1] != 0)
		{

			roomMap[1][5] = 0;
			roomMap[2][5] = 0;
			roomMap[3][5] = 0;
			roomMap[4][5] = 0;
			roomMap[5][5] = 0;

			roomMap[1][4] = 31;
			TMP2.roomObjectPosX = 1 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 113;
			roomObjects.push_back(TMP2);

			roomMap[2][4] = 31;
			TMP2.roomObjectPosX = 2 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 113;
			roomObjects.push_back(TMP2);

			roomMap[3][4] = 31;
			TMP2.roomObjectPosX = 3 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 113;
			roomObjects.push_back(TMP2);

			if (roomMap[4][4] == 30)
			{
				roomMap[4][4] = 27;

				TMP2.roomObjectPosX = 4 * w;
				TMP2.roomObjectPosY = 4 * h;
				TMP2.roomObjectID = 109;
				roomObjects.push_back(TMP2);
			}
			if (state[0] == 0)
			{
				roomMap[5][4] = 31;

				TMP2.roomObjectPosX = 5 * w;
				TMP2.roomObjectPosY = 4 * h;
				TMP2.roomObjectID = 113;
				roomObjects.push_back(TMP2);
			}


			roomMap[1][6] = 28;
			TMP2.roomObjectPosX = 1 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

			roomMap[2][6] = 28;
			TMP2.roomObjectPosX = 2 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

			roomMap[3][6] = 28;
			TMP2.roomObjectPosX = 3 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

			roomMap[4][6] = 28;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

			if (state[3] == 0)
			{
				roomMap[5][6] = 28;
				TMP2.roomObjectPosX = 5 * w;
				TMP2.roomObjectPosY = 6 * h;
				TMP2.roomObjectID = 110;
				roomObjects.push_back(TMP2);
			}
		}

		if (state[2] != 0)
		{

			roomMap[9][5] = 0;
			roomMap[8][5] = 0;
			roomMap[7][5] = 0;
			roomMap[6][5] = 0;
			roomMap[5][5] = 0;

			roomMap[9][4] = 31;
			TMP2.roomObjectPosX = 9 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 113;
			roomObjects.push_back(TMP2);

			roomMap[8][4] = 31;
			TMP2.roomObjectPosX = 8 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 113;
			roomObjects.push_back(TMP2);

			roomMap[7][4] = 31;
			TMP2.roomObjectPosX = 7 * w;
			TMP2.roomObjectPosY = 4 * h;
			TMP2.roomObjectID = 113;
			roomObjects.push_back(TMP2);

			if (roomMap[6][4] == 29)
			{
				roomMap[6][4] = 26;
				TMP2.roomObjectPosX = 6 * w;
				TMP2.roomObjectPosY = 4 * h;
				TMP2.roomObjectID = 108;
				roomObjects.push_back(TMP2);
			}

			if (state[3] == 0 && state[1] == 0)
			{
				roomMap[5][6] = 28;
				TMP2.roomObjectPosX = 5 * w;
				TMP2.roomObjectPosY = 6 * h;
				TMP2.roomObjectID = 110;
				roomObjects.push_back(TMP2);
			}

			roomMap[9][6] = 28;
			TMP2.roomObjectPosX = 9 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

			roomMap[8][6] = 28;
			TMP2.roomObjectPosX = 8 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

			roomMap[7][6] = 28;
			TMP2.roomObjectPosX = 7 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

			roomMap[6][6] = 28;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 6 * h;
			TMP2.roomObjectID = 110;
			roomObjects.push_back(TMP2);

		}

		if (state[3] != 0)
		{
			roomMap[5][9] = 0;
			roomMap[5][8] = 0;
			roomMap[5][7] = 0;
			roomMap[5][6] = 0;
			roomMap[5][5] = 0;

			roomMap[4][9] = 30;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 9 * h;
			TMP2.roomObjectID = 112;
			roomObjects.push_back(TMP2);

			roomMap[4][8] = 30;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 8 * h;
			TMP2.roomObjectID = 112;
			roomObjects.push_back(TMP2);

			roomMap[4][7] = 30;
			TMP2.roomObjectPosX = 4 * w;
			TMP2.roomObjectPosY = 7 * h;
			TMP2.roomObjectID = 112;
			roomObjects.push_back(TMP2);

			if (roomMap[4][6] == 28)
			{
				roomMap[4][6] = 25;
				TMP2.roomObjectPosX = 4 * w;
				TMP2.roomObjectPosY = 6 * h;
				TMP2.roomObjectID = 107;
				roomObjects.push_back(TMP2);
			}
			else
			{
				roomMap[4][6] = 30;
				TMP2.roomObjectPosX = 4 * w;
				TMP2.roomObjectPosY = 6 * h;
				TMP2.roomObjectID = 112;
				roomObjects.push_back(TMP2);
			}

			roomMap[6][9] = 29;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 9 * h;
			TMP2.roomObjectID = 111;
			roomObjects.push_back(TMP2);

			roomMap[6][8] = 29;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 8 * h;
			TMP2.roomObjectID = 111;
			roomObjects.push_back(TMP2);

			roomMap[6][7] = 29;
			TMP2.roomObjectPosX = 6 * w;
			TMP2.roomObjectPosY = 7 * h;
			TMP2.roomObjectID = 111;
			roomObjects.push_back(TMP2);

			if (roomMap[6][6] == 28)
			{
				roomMap[6][6] = 24;
				TMP2.roomObjectPosX = 6 * w;
				TMP2.roomObjectPosY = 6 * h;
				TMP2.roomObjectID = 106;
				roomObjects.push_back(TMP2);
			}
			else
			{
				roomMap[6][6] = 29;
				TMP2.roomObjectPosX = 6 * w;
				TMP2.roomObjectPosY = 6 * h;
				TMP2.roomObjectID = 111;
				roomObjects.push_back(TMP2);
			}

		}

		for (int i = 1; i < tileAmount - 1; i++)
		{
			for (int j = 1; j < tileAmount - 1; j++)
			{
				if (roomMap[j][i] == 99)
					roomMap[j][i] = 32;
			}
		}

		return;
		break;
	}
	case 6:
	{
		roomMap[2][2] = 37;
		RoomObjectsInfo TMP2(2 * w, 2 * h, 115);
		roomObjects.push_back(TMP2);

		roomMap[4][2] = 37;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 2 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[3][3] = 37;
		TMP2.roomObjectPosX = 3 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[2][4] = 37;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[4][4] = 37;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);


		roomMap[6][2] = 37;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 2 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[8][2] = 37;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 2 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[7][3] = 37;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[6][4] = 37;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[8][4] = 37;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);


		roomMap[2][6] = 37;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[4][6] = 37;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[3][7] = 37;
		TMP2.roomObjectPosX = 3 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[2][8] = 37;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[4][8] = 37;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);


		roomMap[6][6] = 37;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[8][6] = 37;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[7][7] = 37;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[6][8] = 37;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);

		roomMap[8][8] = 37;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 115;
		roomObjects.push_back(TMP2);


		return;
		break;
	}
	case 7:
	{
		roomMap[3][2] = 20;
		RoomObjectsInfo TMP2(3 * w, 2 * h, 102);
		roomObjects.push_back(TMP2);

		roomMap[7][2] = 20;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 2 * h;
		TMP2.roomObjectID = 102;
		roomObjects.push_back(TMP2);

		roomMap[2][3] = 24;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 106;
		roomObjects.push_back(TMP2);

		roomMap[3][3] = 32;

		roomMap[4][3] = 25;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 107;
		roomObjects.push_back(TMP2);

		roomMap[6][3] = 24;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 106;
		roomObjects.push_back(TMP2);

		roomMap[7][3] = 32;

		roomMap[8][3] = 25;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 107;
		roomObjects.push_back(TMP2);

		roomMap[2][4] = 29;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 111;
		roomObjects.push_back(TMP2);

		roomMap[3][4] = 32;

		roomMap[4][4] = 32;

		roomMap[5][4] = 28;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 110;
		roomObjects.push_back(TMP2);

		roomMap[6][4] = 32;

		roomMap[7][4] = 32;

		roomMap[8][4] = 30;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 4 * h;
		TMP2.roomObjectID = 112;
		roomObjects.push_back(TMP2);

		roomMap[2][5] = 26;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 108;
		roomObjects.push_back(TMP2);

		roomMap[3][5] = 32;

		roomMap[4][5] = 32;

		roomMap[5][5] = 32;

		roomMap[6][5] = 32;

		roomMap[7][5] = 32;

		roomMap[8][5] = 27;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);

		roomMap[3][6] = 26;
		TMP2.roomObjectPosX = 3 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 108;
		roomObjects.push_back(TMP2);

		roomMap[4][6] = 32;

		roomMap[5][6] = 32;

		roomMap[6][6] = 32;

		roomMap[7][6] = 27;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 6 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);

		roomMap[4][7] = 26;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 108;
		roomObjects.push_back(TMP2);

		roomMap[5][7] = 32;

		roomMap[6][7] = 27;
		TMP2.roomObjectPosX = 6 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);

		roomMap[5][8] = 23;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 8 * h;
		TMP2.roomObjectID = 105;
		roomObjects.push_back(TMP2);

		return;
		break;
	}
	case 8:
	{
		if (state[1] != 0)
			return;

		roomMap[1][1] = 21;
		RoomObjectsInfo TMP2(1 * w, 1 * h, 103);
		roomObjects.push_back(TMP2);

		roomMap[2][1] = 25;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 1 * h;
		TMP2.roomObjectID = 107;
		roomObjects.push_back(TMP2);

		roomMap[2][2] = 32;

		roomMap[1][3] = 24;
		TMP2.roomObjectPosX = 1 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 106;
		roomObjects.push_back(TMP2);

		roomMap[2][3] = 27;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);

		roomMap[4][3] = 21;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 103;
		roomObjects.push_back(TMP2);

		roomMap[5][3] = 25;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 3 * h;
		TMP2.roomObjectID = 107;
		roomObjects.push_back(TMP2);

		roomMap[1][4] = 32;
		roomMap[5][4] = 32;

		roomMap[1][5] = 26;
		TMP2.roomObjectPosX = 1 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 108;
		roomObjects.push_back(TMP2);

		roomMap[2][5] = 22;
		TMP2.roomObjectPosX = 2 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 104;
		roomObjects.push_back(TMP2);

		roomMap[4][5] = 21;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 103;
		roomObjects.push_back(TMP2);

		roomMap[5][5] = 30;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 112;
		roomObjects.push_back(TMP2);

		roomMap[7][5] = 24;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 106;
		roomObjects.push_back(TMP2);

		roomMap[8][5] = 22;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 5 * h;
		TMP2.roomObjectID = 104;
		roomObjects.push_back(TMP2);

		roomMap[5][6] = 32;

		roomMap[7][6] = 32;

		roomMap[4][7] = 21;
		TMP2.roomObjectPosX = 4 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 103;
		roomObjects.push_back(TMP2);

		roomMap[5][7] = 27;
		TMP2.roomObjectPosX = 5 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);

		roomMap[7][7] = 26;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 108;
		roomObjects.push_back(TMP2);

		roomMap[8][7] = 25;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 7 * h;
		TMP2.roomObjectID = 107;
		roomObjects.push_back(TMP2);

		roomMap[8][8] = 32;

		roomMap[7][9] = 21;
		TMP2.roomObjectPosX = 7 * w;
		TMP2.roomObjectPosY = 9 * h;
		TMP2.roomObjectID = 103;
		roomObjects.push_back(TMP2);

		roomMap[8][9] = 27;
		TMP2.roomObjectPosX = 8 * w;
		TMP2.roomObjectPosY = 9 * h;
		TMP2.roomObjectID = 109;
		roomObjects.push_back(TMP2);


		break;
	}

	}

}

void GameMapRoom::GenerateMoney(int gStatus)
{
	srand(time(0));
	int TMP;
	int x, y;
	int w, h;
	TMP = rand() % 4 + 1;

	if (gStatus == 0)
	{
		w = 72;
		h = 54;
	}
	else if (gStatus == 1)
	{
		w = 92;
		h = 69;
	}
	else if (gStatus == 2)
	{
		w = 116;
		h = 87;
	}
	else if (gStatus == 3)
	{
		w = 128;
		h = 96;
	}

	while (TMP > 0)
	{
		x = rand() % 800 + w;
		y = rand() % 600 + h;

		if (roomMap[x / w][y / h] != 0)
			continue;

		RoomObjectsInfo TMP2(x, y, 100);
		roomObjects.push_back(TMP2);
		TMP--;
	}
}

void GameMapRoom::GenerateHearts(int gStatus)
{
	srand(time(0));
	int TMP = 1;
	int x, y;
	int w, h;


	if (gStatus == 0)
	{
		w = 72;
		h = 54;
	}
	else if (gStatus == 1)
	{
		w = 92;
		h = 69;
	}
	else if (gStatus == 2)
	{
		w = 116;
		h = 87;
	}
	else if (gStatus == 3)
	{
		w = 128;
		h = 96;
	}

	while (TMP > 0)
	{
		x = rand() % 800 + w;
		y = rand() % 600 + h;

		if (roomMap[x / w][y / h] != 0)
			continue;

		RoomObjectsInfo TMP2(x, y, 101);
		roomObjects.push_back(TMP2);
		TMP--;
	}
}

GameMapRoom** GameMapRoom::GetDoors()
{
	return doors;
}

int GameMapRoom::GetRoomMapTileID(int x, int y)
{
	return roomMap[x][y];
}

int GameMapRoom::GetRoomObjectID(int i)
{
	return roomObjects[i].roomObjectID;
}

int GameMapRoom::GetRoomObjectsSize()
{
	return roomObjects.size();
}

int GameMapRoom::GetRoomObjectPosX(int i)
{
	return roomObjects[i].roomObjectPosX;
}

int GameMapRoom::GetRoomObjectPosY(int i)
{
	return roomObjects[i].roomObjectPosY;
}

void GameMapRoom::SetRoomObjectColliderPosX(int i, int x)
{
	roomObjects[i].colliderPosX = x;
}

void GameMapRoom::SetRoomObjectColliderPosY(int i, int y)
{
	roomObjects[i].colliderPosY = y;
}

void GameMapRoom::SetRoomObjectPosX(int i, int x)
{
	roomObjects[i].colliderPosX = x;
}

void GameMapRoom::SetRoomObjectPosY(int i, int y)
{
	roomObjects[i].colliderPosY = y;
}

void GameMapRoom::SetRoomObjectColliderWidth(int i, int w)
{
	roomObjects[i].colliderW = w;
}

void GameMapRoom::SetRoomObjectColliderHeight(int i, int h)
{
	roomObjects[i].colliderH = h;
}

int GameMapRoom::GetRoomObjectColliderWidth(int i)
{
	return roomObjects[i].colliderW;
}

int GameMapRoom::GetRoomObjectColliderHeight(int i)
{
	return roomObjects[i].colliderH;
}

void GameMapRoom::EraseRoomObject(int i)
{
	roomObjects.erase(roomObjects.begin() + i);
}

int GameMapRoom::GetRoomObjectColliderPosX(int i)
{
	return roomObjects[i].colliderPosX;
}

int GameMapRoom::GetRoomObjectColliderPosY(int i)
{
	return roomObjects[i].colliderPosY;
}