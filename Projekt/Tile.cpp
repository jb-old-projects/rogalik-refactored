/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "Tile.h"

Tile::Tile(TileType type, bool isWalkable, std::string bmpName)
{
	this->type = type;
	this->isWalkable = isWalkable;

	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	tileSurface = IMG_LoadBMP_RW(rwop);
}

Tile::~Tile()
{
	SDL_RWclose(rwop);
	SDL_FreeSurface(tileSurface);
}

bool Tile::IsWalkAvailable(int type)
{
	if (type == TileType::Floor || type == TileType::UpperDoors || type == TileType::BottomDoors
		|| type == TileType::LeftDoors || type == TileType::RightDoors)
	{
		return true;
	}
	else
	{
		return false;
	}
}

SDL_Surface* Tile::GetTileSurface()
{
	return tileSurface;
}

void Tile::SetTileSurface(SDL_Surface* s)
{
	tileSurface = s;
}