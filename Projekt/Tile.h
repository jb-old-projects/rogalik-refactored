/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#pragma comment (lib, "SDL2")
#pragma comment (lib, "SDL2_image")
#include <string>
#include <SDL.h>
#include <SDL2\SDL_image.h>

class Tile
{
public:

	enum TileType
	{
		Floor,
		UpperWall,
		LeftWall,
		RightWall,
		BottomWall,
		UpperDoors,
		LeftDoors,
		RightDoors,
		BottomDoors,
		UpperCornerL,
		UpperCornerR,
		BottomCornerL,
		BottomCornerR,
		Menu,
		Ending,
		MenuDot,
		Pause,
		Player,
		MenuPauseDot,
		Hole,
		Hole_3x1,
		Hole_3x2,
		Hole_3x3,
		Hole_3x4,
		Hole_2x1,
		Hole_2x2,
		Hole_2x3,
		Hole_2x4,
		Hole_1x1,
		Hole_1x2,
		Hole_1x3,
		Hole_1x4,
		hole_0,
		HoleLinkUL,
		HoleLinkUR,
		HoleLinkDL,
		HoleLinkDR,
		BigHole,
		Stats
	};

	static bool IsWalkAvailable(int tileID);	// Statyczna metoda sprawdzj�ca, czy jest mo�liwe chodzenie po kafelku o zadanym ID

	Tile();
	Tile(TileType type, bool isWalkable, std::string bmpName);
	virtual ~Tile();

	SDL_Surface* GetTileSurface();

	void SetTileSurface(SDL_Surface* s);

protected:

	SDL_Surface* tileSurface = NULL;
	SDL_RWops* rwop = NULL;

	TileType type;
	bool isWalkable;
};