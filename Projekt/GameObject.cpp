/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "GameObject.h"

GameObject::GameObject(GameObjectID goID, GameObjectType goType, std::string bmpName, int goWidth, int goHeight)
{
	this->goID = goID;
	this->goType = goType;
	this->rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	this->goSurface = IMG_LoadPNG_RW(rwop);

	this->goRect.w = goWidth;
	this->goRect.h = goHeight;
}

GameObject::~GameObject()
{
	SDL_RWclose(rwop);
	SDL_FreeSurface(goSurface);
}

void GameObject::EditSurface(std::string bmpName, int goWidth, int goHeight)
{
	this->rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	this->goSurface = IMG_LoadPNG_RW(rwop);

	this->goRect.w = goWidth;
	this->goRect.h = goHeight;
}

void GameObject::SetRectY(int y)
{
	goRect.y = y;
}

void GameObject::SetRectX(int x)
{
	goRect.x = x;
}

SDL_Rect GameObject::GetRect()
{
	return goRect;
}

SDL_Surface* GameObject::GetSurface()
{
	return goSurface;
}