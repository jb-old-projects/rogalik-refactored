/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#pragma once
#pragma comment (lib, "SDL2_image")
#include <SDL.h>
#include <SDL2\SDL_image.h>
#include <string>

class GameObject
{
public:

#pragma region Custom Enumeration Types

	enum GameObjectID
	{
		e_money = 100,
		e_heart = 101,
		e_hole_1,
		e_hole_2,
		e_hole_3,
		e_hole_4,
		e_hole_5,
		e_hole_6,
		e_hole_7,
		e_hole_8,
		e_hole_9,
		e_hole_10,
		e_hole_11,
		e_hole_12,
		e_hole_13,
		e_hole_14
	};

	enum GameObjectType
	{
		e_neutral,
		e_harmfull,
		e_deadly,
	};

#pragma endregion

	GameObject(GameObjectID goID, GameObjectType goType, std::string bmpName, int goWidth, int goHeight);
	virtual ~GameObject();
	void EditSurface(std::string bmpName, int goWidth, int goHeight);	// Metoda zmieniaj�ca surface kafelka przy zmianie rozdzielczo�ci gry

	SDL_Rect GetRect();
	SDL_Surface* GetSurface();

	void SetRectY(int y);
	void SetRectX(int x);

protected:

	SDL_Surface* goSurface = NULL;
	SDL_RWops* rwop = NULL;
	GameObjectID goID;
	GameObjectType goType;
	SDL_Rect goRect;
};