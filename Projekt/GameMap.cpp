/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "GameMap.h"

int GameMap::roomAmount = 0;

GameMap::GameMap()
{
	tileSizeW = 72;
	tileSizeH = 54;
	tileAmount = 11;
	mapH = tileSizeH * tileAmount;
	mapW = tileSizeW * tileAmount;
	mapSize = mapH * mapW;
	currentRoom = new GameMapRoom(99, 0);
}

GameMap::~GameMap()
{
	delete currentRoom;
}

GameMapRoom* GameMap::GetCurrentRoom()
{
	return currentRoom;
}

void GameMap::SetCurrentRoom(GameMapRoom* room)
{
	currentRoom = room;
}

void GameMap::SetRoomAmount(int a)
{
	roomAmount = a;
}

int GameMap::GetRoomAmount()
{
	return roomAmount;
}

int GameMap::GetTileSizeW()
{
	return tileSizeW;
}

int GameMap::GetTileSizeH()
{
	return tileSizeH;
}

int GameMap::GetTileAmount()
{
	return tileAmount;
}

void GameMap::SetTileSizeW(int w)
{
	tileSizeW = w;
}

void GameMap::SetTileSizeH(int h)
{
	tileSizeH = h;
}

void GameMap::SetTileAmount(int a)
{
	tileAmount = a;
}

int GameMap::GetMapH()
{
	return mapH;
}

int GameMap::GetMapW()
{
	return mapW;
}

int GameMap::GetMapSize()
{
	return mapSize;
}

void GameMap::SetMapH(int h)
{
	mapH = h;
}

void GameMap::SetMapW(int w)
{
	mapW = w;
}

void GameMap::SetMapSize(int s)
{
	mapSize = s;
}