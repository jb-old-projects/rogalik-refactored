/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "AnimatedTile.h"

AnimatedTile::AnimatedTile(TileType tileID, bool isWalkable, std::string bmpName, int x, int y, int w, int h)
	: Tile(tileID, isWalkable, bmpName)
{
	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	tileSurface = IMG_LoadPNG_RW(rwop);

	tileRect.x = x;
	tileRect.y = y;
	tileRect.w = w;
	tileRect.h = h;
}

SDL_Rect AnimatedTile::GetTileRect()
{
	return tileRect;
}
void AnimatedTile::SetTileRectX(int x)
{
	tileRect.x = x;
}

void AnimatedTile::SetTileRectY(int y)
{
	tileRect.y = y;
}