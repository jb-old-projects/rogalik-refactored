/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Rogalik
/* Przedmiot: Programowanie Komputer�w 3
/* Prowadz�cy: mgr in�. Krzysztof Pawe�czyk
/* Data ostatniej modyfikacji: 5 lutego 2018
/**********************************************/

#include "AnimatedGameObject.h"

AnimatedGameObject::AnimatedGameObject(GameObjectID id, GameObjectType gameObjectType, std::string bmpName, int gameObjectW, int gameObjectH)
	: GameObject(id, gameObjectType, bmpName, gameObjectW, gameObjectH)
{
	gameObjectAnim.x = 0;
	gameObjectAnim.y = 0;
	gameObjectAnim.w = goRect.w;
	gameObjectAnim.h = goRect.h;
}

void AnimatedGameObject::EditSurface(std::string bmpName, int width, int height)
{
	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	goSurface = IMG_LoadPNG_RW(rwop);

	goRect.w = width;
	goRect.h = height;
	gameObjectAnim.x = 0;
	gameObjectAnim.y = 0;
	gameObjectAnim.w = goRect.w;
	gameObjectAnim.h = goRect.h;
}

SDL_Rect AnimatedGameObject::GetGameObjectAnim()
{
	return gameObjectAnim;
}

int AnimatedGameObject::GetAnimationStateCounter()
{
	return animationStateCounter;
}

void AnimatedGameObject::SetAnimationStateCounter(int animationStateCounter)
{
	this->animationStateCounter = animationStateCounter;
}

int AnimatedGameObject::GetGameObjectAnimPosX()
{
	return gameObjectAnim.x;
}

void AnimatedGameObject::SetGameObjectAnimPosX(int x)
{
	this->gameObjectAnim.x = x;
}

int AnimatedGameObject::GetGameObjectAnimPosY()
{
	return gameObjectAnim.y;
}

void AnimatedGameObject::SetGameObjectAnimPosY(int y)
{
	this->gameObjectAnim.y = y;
}